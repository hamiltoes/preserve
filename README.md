# preserve

> A self-hosted Google Keep alternative built with Vue, Quasar and Feathers

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and yarn installed.
2. Install your dependencies (including capcitor dependencies)

   ```
   cd path/to/preserve
   yarn install

   cd src-capacitor
   yarn install
   ```

3. You will also need a Postgres DB set up. Modify `./config/default.json` to use your DB credentials.

### Developing

3. Start the backend in dev mode

   ```
   yarn dev:back
   ```

4. Start the frontend in dev mode (with hot reloading)

   ```
   yarn dev:front
   ```

### Running

3. Build the production frontend

   ```bash
   yarn build
   ```

4. Compile and run the production backend

   ```bash
   yarn start
   ```

### Lint the files

```bash
yarn lint --fix
```
