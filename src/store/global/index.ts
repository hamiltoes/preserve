import { Module } from "vuex";

import { assertBool } from "common/asserts";
import { StoreModuleState } from "src/store";

const state = {
    connectedToApi: false
}
type GlobalState = typeof state;

declare module "../" {
    interface StoreModuleState {
        global: GlobalState
    }
}

const globalModule: Module<GlobalState, StoreModuleState> = {
    namespaced: true,
    state,
    getters: {
        connected({ connectedToApi }) {
            return connectedToApi;
        }
    },
    mutations: {
        setConnected(state, payload) {
            assertBool(payload)
            state.connectedToApi = payload
        }
    }
}

export default globalModule
