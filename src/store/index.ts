import Vue from "vue";
import Vuex, { Store } from "vuex";
import auth from "./store.auth";
import globalModule from "./global";
import servicePlugins from "./services";
import { FeathersVuex } from "src/feathers-client";

Vue.use(Vuex);
Vue.use(FeathersVuex);

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ModelApis { /* Let each service augment this interface */ }
declare module "feathers-vuex" {
  interface FeathersVuexGlobalModels {
    "api": ModelApis
  }
}

export type StoreInterface = Store<StoreModuleState>

export default new Vuex.Store({
  modules: { global: globalModule },
  plugins: [...servicePlugins, auth],
  strict: true
}) as StoreInterface;
