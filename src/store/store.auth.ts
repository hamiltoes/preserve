import { HookContext } from "@feathersjs/feathers";
import { FeathersError } from "@feathersjs/errors";
import { paramsForServer } from "feathers-hooks-common";

import feathersClient, { makeAuthPlugin } from "../feathers-client";
import { UserWithSettings, IJwtPayload } from "src-srv/types";

export default makeAuthPlugin({ userService: "users" });

declare module "./" {
    interface StoreModuleState {
        auth: {
            accessToken: string;
            payload: IJwtPayload;
            entityIdField: string;
            responseEntityField: string;
            isAuthenticatePending: boolean;
            isLogoutPending: boolean;
            errorOnAuthenticate: FeathersError;
            errorOnLogout: FeathersError;
            user: UserWithSettings;
            serverAlias: string;
        }
    }
}

// Allow passing `deviceDetails` in params
feathersClient.service("authentication").hooks({
    before: {
        create: [
            (ctx: HookContext) => {
                ctx.params = paramsForServer(ctx.params, "deviceDetails")
            }
        ]
    }
});
