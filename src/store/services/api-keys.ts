// src/store/services/users.js
import { ServiceState } from "feathers-vuex";

import feathersClient, {
  makeServicePlugin,
  BaseModel,
} from "../../feathers-client";

import { ApiKeyRecord, ApiKeyPayload } from "src-srv/types";

interface ReplacementTypes {
  /** Date of expiration */
  expiresAt: Date;
  /** Date of issue */
  issuedAt: Date;
  /** Date of last use */
  lastUsedAt: Date;
}

type additionalTypes = Pick<ApiKeyPayload, "description" | "limit">;

export interface ApiKey extends Omit<ApiKeyRecord, keyof ReplacementTypes>, ReplacementTypes, additionalTypes {}
export class ApiKey extends BaseModel {
  constructor(data: ApiKeyRecord | ApiKey, options = {}) {
    super(data, options);
  }
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "ApiKey";
  // Define default properties here
  static instanceDefaults(): Partial<ApiKeyRecord> {
    return {
      id: undefined,
      userId: undefined,

      accessToken: "",
      expiresAt: 0,
      hits: 0,
      issuedAt: 0,
      lastUsedAt: 0,
      payload: {
        description: "",
        exp: 0,
        iat: 0,
        isApiKey: true,
        jti: "",
        limit: 0,
        scope: {
          "list-entries": [],
          notes: []
        },
        sub: "0"
      }
    };
  }

  /**
   * Setup a new ApiKey instance
   * @param {ApiKey} data ApiKey data from API server
   * @param {*} param1.store Vuex store
   * @param {*} param1.models Global FeathersVuex Models
   */
  static setupInstance(data: Partial<ApiKeyRecord | ApiKey>) {
    const final = data as ApiKey;
    if (data.expiresAt && data.expiresAt > 0)
      final.expiresAt = new Date(data.expiresAt);
    if (data.issuedAt && data.issuedAt > 0)
      final.issuedAt = new Date(data.issuedAt);
    if (data.lastUsedAt && data.lastUsedAt > 0)
      final.lastUsedAt = new Date(data.lastUsedAt);
    // Pull fields from payload to top level
    if (data.payload) {
      final.description = data.payload.description;
      final.limit = data.payload.limit;
    }
    return data;
  }
}
const servicePath = "api-key";
const servicePlugin = makeServicePlugin({
  Model: ApiKey,
  service: feathersClient.service(servicePath),
  servicePath
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;

declare module "../" {
  interface StoreModuleState {
    [servicePath]: ServiceState<ApiKey>
  }
}
declare module "src/store" {
  interface ModelApis {
    ApiKey: typeof ApiKey
  }
}
