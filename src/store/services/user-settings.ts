// src/store/services/note.js
import { ServiceState } from "feathers-vuex";

import feathersClient, {
  makeServicePlugin,
  AutoSaveBaseModel,
  makeAutoSaveServiceExtension,
} from "../../feathers-client";

import { UserSettingsRecord } from "src-srv/types";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface UserSetting extends UserSettingsRecord {}
export class UserSetting extends AutoSaveBaseModel {

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "UserSetting";
  // Define default properties here
  static instanceDefaults() {
    return {
      id: null,
      userId: null,

      allowExpandChecklistInPreview: false,
      darkTheme: false,
      displayGrid: false,
      geotagNotes: false
    };
  }

  /**
   * Setup a new UserSetting instance
   * @param {UserSetting} data UserSetting data from API server
   * @param {*} param1.store Vuex store
   * @param {*} param1.models Global FeathersVuex Models
   */
  static setupInstance(data: Partial<UserSettingsRecord>) {
    return data;
  }
}

const servicePath = "user-settings";
const servicePlugin = makeServicePlugin({
  Model: UserSetting,
  service: feathersClient.service(servicePath),
  servicePath,
  ...makeAutoSaveServiceExtension({
    Model: UserSetting,
    service: feathersClient.service(servicePath)
  })
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;

declare module "../" {
  interface StoreModuleState {
    [servicePath]: ServiceState<UserSetting>
  }
}

declare module "src/store" {
  interface ModelApis {
    UserSetting: typeof UserSetting
  }
}
