// src/store/services/note.js
import type { Hook } from "@feathersjs/feathers";
import { ServiceState } from "feathers-vuex";

import feathersClient, {
  makeServicePlugin,
  AutoSaveBaseModel,
  makeAutoSaveServiceExtension,
} from "../../feathers-client";

import { ListEntryRecord } from "src-srv/types";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ListEntry extends ListEntryRecord {}
export class ListEntry extends AutoSaveBaseModel {

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "ListEntry";
  // Define default properties here
  static instanceDefaults(): Partial<ListEntryRecord> {
    return {
      id: undefined,
      noteId: undefined,
      userId: undefined,

      done: false,
      text: "",
    };
  }

  /**
 * Setup a new ListEntry instance
 * @param data ListEntry data from API server
 */
  static setupInstance(data: Partial<ListEntry | ListEntryRecord> /*, { store, models }*/) {
    return data;
  }

  /**
 * Copy Model fields from `a` to `b`
 * @param a source Model
 * @param b dest Model
 */
  static copy(a: Partial<ListEntryRecord>, b: Partial<ListEntryRecord>) {
    b.done = a?.done;
    b.text = a?.text;
    b.userId = a?.userId;
  }
}

const servicePath = "list-entries";
const servicePlugin = makeServicePlugin({
  Model: ListEntry,
  service: feathersClient.service(servicePath),
  servicePath,
  ...makeAutoSaveServiceExtension({
    Model: ListEntry,
    service: feathersClient.service(servicePath)
  })
});

const prune: Hook = (ctx) => {
  if (ctx.result && ctx.params.pruneApiResponse) {
    const { id } = ctx.result;
    ctx.result = { id };
  }
  return ctx;
}

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [prune],
    update: [prune],
    patch: [prune],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;

declare module "../" {
  interface StoreModuleState {
    [servicePath]: ServiceState<ListEntry>
  }
}

declare module "src/store" {
  interface ModelApis {
    ListEntry: typeof ListEntry
  }
}
