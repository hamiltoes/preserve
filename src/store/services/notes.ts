// src/store/services/note.js
import type { Hook } from "@feathersjs/feathers";
import { ServiceState } from "feathers-vuex";
import { EventEmitter } from "events"

import feathersClient, {
  makeServicePlugin,
  AutoSaveBaseModel,
  makeAutoSaveServiceExtension,
} from "../../feathers-client";
import { GeolocationCoords } from "src/composables/use-geolocation";
import { NoteRecord } from "src-srv/types";

interface ReplacementTypes {
  /** Date of creation */
  createdAt: Date;
  /** Date of last update */
  updatedAt: Date;
  /** Note's geotag */
  coords: GeolocationCoords;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Note extends Omit<NoteRecord, keyof ReplacementTypes>, ReplacementTypes {}
export class Note extends AutoSaveBaseModel {

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Note";
  // Define default properties here
  static instanceDefaults(): Partial<NoteRecord> {
    return {
      id: undefined,
      userId: undefined,

      archived: false,
      checkboxes: false,
      color: "default",
      createdAt: undefined,
      starred: false,
      text: "",
      title: "",
      trashed: 0, // Not a boolean; this is epoch time the Note was moved to trash
      updatedAt: undefined
    };
  }

  /**
   * Setup a new Note instance
   * @param data Note data from API server
   */
  static setupInstance(data: Partial<Note | NoteRecord> /*, { store, models }*/) {
    if (data.createdAt && data.createdAt > 0)
      data.createdAt = new Date(data.createdAt)
    if (data.updatedAt && data.updatedAt > 0)
      data.updatedAt = new Date(data.updatedAt)
    return data;
  }

  /**
 * Copy Model fields from `a` to `b`
 * @param a source Model
 * @param b dest Model
 */
  static copy(a: Partial<Note>, b: Partial<Note>) {
    b.archived = a?.archived;
    b.checkboxes = a?.checkboxes;
    b.color = a?.color;
    b.starred = a?.starred;
    b.text = a?.text;
    b.title = a?.title;
    b.trashed = a?.trashed;
    b.userId = a?.userId;
  }
}

const servicePath = "notes";
const servicePlugin = makeServicePlugin({
  Model: Note,
  service: feathersClient.service(servicePath),
  servicePath,
  ...makeAutoSaveServiceExtension({
    Model: Note,
    service: feathersClient.service(servicePath)
  })
});

const prune: Hook = (ctx) => {
  if (ctx.result && ctx.params.pruneApiResponse) {
    const { id, createdAt, updatedAt } = ctx.result;
    ctx.result = { id, createdAt, updatedAt };
  }
  return ctx;
}

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [prune],
    update: [prune],
    patch: [prune],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;

(Note as unknown as EventEmitter).setMaxListeners(1000)

declare module "../" {
  interface StoreModuleState {
    [servicePath]: ServiceState<Note>
  }
}

declare module "src/store" {
  interface ModelApis {
    Note: typeof Note
  }
}
