import apiKeysPlugin from "./api-keys";
import listEntriesPlugin from "./list-entries";
import notesPlugin from "./notes";
import userSettingsPlugin from "./user-settings";
import usersPlugin from "./users";

export default [
    apiKeysPlugin,
    listEntriesPlugin,
    notesPlugin,
    userSettingsPlugin,
    usersPlugin
]
