// src/store/services/users.js
import crypto from "crypto";
import { ServiceState } from "feathers-vuex";

import feathersClient, {
  makeServicePlugin,
  BaseModel,
} from "../../feathers-client";

import { UserSetting } from "./user-settings";
import { FeathersVuexGlobalModels } from "feathers-vuex";
import { UserWithSettings, UserRecord } from "src-srv/types";

// from FeathersVuex make-base-model.ts. Can remove after converting feathers-client.js to ts
interface BaseModelInstanceOptions {
  clone?: boolean
  commit?: boolean
  merge?: boolean
}

// The Gravatar image service
const gravatarUrl = "https://s.gravatar.com/avatar";
// The size query. Our chat needs 60px images
const query = "s=60";

interface ReplacementTypes {
  settings?: UserSetting;
  /** Date of creation */
  createdAt: Date;
  /** Date of last update */
  updatedAt: Date;
}

export interface User extends Omit<UserWithSettings, keyof ReplacementTypes>, ReplacementTypes {}
export class User extends BaseModel {
  constructor(data: Partial<UserWithSettings | User | UserRecord>, options?: BaseModelInstanceOptions) {
    super(data, options);
  }
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "User";
  // Define default properties here
  static instanceDefaults() {
    return {
      createdAt: null,
      email: "",
      password: "",
      updatedAt: null
    };
  }

  /**
   * Setup a new User instance
   * @param data User data from API server
   * @param param1.models Global FeathersVuex Models
   */
  static setupInstance(data: Partial<UserWithSettings | ReplacementTypes>, { models }: { models: FeathersVuexGlobalModels }) {
    if (data.createdAt && data.createdAt > 0)
      data.createdAt = new Date(data.createdAt)
    if (data.updatedAt && data.updatedAt > 0)
      data.updatedAt = new Date(data.updatedAt)
    if (data.settings)
      data.settings = new models.api.UserSetting(data.settings);
    return data;
  }

  /** Gravatar URL */
  get avatar() {
    // Gravatar uses MD5 hashes from an email address (all lowercase) to get the image
    const hash = crypto.createHash("md5").update(this.email.toLowerCase()).digest("hex");
    return `${gravatarUrl}/${hash}?${query}`;
  }
}
const servicePath = "users";
const servicePlugin = makeServicePlugin({
  Model: User,
  service: feathersClient.service(servicePath),
  servicePath
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;

declare module "../" {
  interface StoreModuleState {
    [servicePath]: ServiceState<User>
  }
}

declare module "src/store" {
  interface ModelApis {
    User: typeof User
  }
}
