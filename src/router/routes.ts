import { RouteParams } from "quasar";
import { NavigationGuard, RouteConfig } from "vue-router";

import { StoreInterface } from "../store";

function getRoutes({ store }: RouteParams<StoreInterface>) {

  // Redirect un-authed users to login
  const preventGuestUser: NavigationGuard = (to, from, next) => {
    if (store.getters["auth/isAuthenticated"]) {
      next();
    } else {
      // set a redirect query so components managing login
      // know where to redirect upon successful login
      next({ name: "Login", query: { redirect: to.fullPath } })
    }
  }

  // Redirect authed users to home
  const preventAuthenticatedUser: NavigationGuard = (to, from, next) => {
    if (store.getters["auth/isAuthenticated"]) {
      next({ name: "Home" })
    } else {
      next();
    }
  }

  const routes: RouteConfig[] = [
    {
      path: "/",
      component: () => import("layouts/MainLayout.vue"),
      beforeEnter: preventGuestUser,
      children: [
        { path: "", redirect: { name: "Home" } },
        { name: "Home", path: "home", component: () => import("pages/Index.vue") },
        { name: "Archive", path: "archive", component: () => import("pages/Archive.vue") },
        { name: "Timeline", path: "timeline", component: () => import("pages/Timeline.vue") },
        { name: "Trash", path: "trash", component: () => import("pages/Trash.vue") }
      ]
    },
    {
      path: "/profile/",
      component: () => import("layouts/MainLayout.vue"),
      beforeEnter: preventGuestUser,
      children: [
        { name: "API Keys", path: "api-keys", component: () => import("pages/ApiKeys.vue") },
      ]
    },
    {
      path: "/login",
      component: () => import("layouts/LoginLayout.vue"),
      beforeEnter: preventAuthenticatedUser,
      children: [
        { name: "Login", path: "", component: () => import("pages/Login.vue") }
      ]
    }
  ];

  // Always leave this as last one
  if (process.env.MODE !== "ssr") {
    routes.push({
      path: "*",
      component: () => import("pages/Error404.vue"),
    });
  }

  return routes
}

export default getRoutes;
