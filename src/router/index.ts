import { route } from "quasar/wrappers";
import VueRouter from "vue-router";

import getRoutes from "./routes"
import { StoreInterface } from "../store";

// Quasar adds $router to store
declare module "vuex/types" {
  interface Store<S> {
    $router: VueRouter
  }
}

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route<StoreInterface>(function (context) {
  context.Vue.use(VueRouter);

  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes: getRoutes(context),

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
})
