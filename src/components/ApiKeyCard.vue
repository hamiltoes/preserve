<template>
  <q-card :bordered="$q.dark.isActive" class="text-color-body-dark">
    <q-card-section
      :class="
        `cursor-pointer q-py-sm ${$q.dark.isActive ? 'bg-dark' : 'bg-primary'}`
      "
      @click="expanded = !expanded"
    >
      <div class="row items-center">
        <q-badge floating :color="badgeColor" :label="badgeText" />
        <div class="col-auto q-pl-sm q-pr-lg ">
          <q-icon
            size="32px"
            :name="expanded ? 'expand_more' : 'chevron_right'"
          />
        </div>
        <div class="col-6">
          <q-input
            dark
            stack-label
            input-class="text-h6 cursor-pointer"
            label="API Key Name"
            :borderless="true"
            :placeholder="apiKey.id"
            :readonly="true"
            :value="apiKey.description"
          />
        </div>
      </div>
    </q-card-section>

    <q-slide-transition>
      <div v-if="expanded">
        <q-card-section class="q-pb-sm">
          <div class="row">
            <div class="col">
              <!-- API Key -->
              <q-input
                outlined
                readonly
                :value="apiKey.accessToken"
                label="API Key"
                class="q-mb-md"
              >
                <template v-slot:append>
                  <q-icon
                    name="assignment"
                    class="cursor-pointer"
                    @click="copyApiKey"
                  >
                    <q-tooltip>Copy to clipboard</q-tooltip>
                  </q-icon>
                </template>
              </q-input>
            </div>
          </div>

          <div class="row">
            <div class="col-4 q-pr-sm">
              <!-- Payload -->
              <q-input
                outlined
                readonly
                :value="JSON.stringify(apiKey.payload, null, '  ')"
                label="JWT Payload"
                type="textarea"
                input-style="height: 245px"
              />
            </div>
            <div class="col-8 q-pl-sm">
              <!-- Times -->
              <div class="row">
                <div class="col-4 q-pr-xs">
                  <q-input
                    outlined
                    stack-label
                    label="Issued At"
                    readonly
                    :value="apiKey.issuedAt.toLocaleString()"
                  />
                </div>
                <div class="col-4 q-px-xs">
                  <q-input
                    outlined
                    stack-label
                    readonly
                    :label="`Expires At (~${expiresIn} left)`"
                    :value="apiKey.expiresAt.toLocaleString()"
                  />
                </div>
                <div class="col-4 q-pl-xs">
                  <q-input
                    outlined
                    stack-label
                    label="Last Used"
                    readonly
                    :value="
                      apiKey.lastUsedAt
                        ? apiKey.lastUsedAt.toLocaleString()
                        : '(unused)'
                    "
                  />
                </div>
              </div>
              <!-- Usage -->
              <div class="row q-mt-sm">
                <div class="col-4 q-pr-xs">
                  <q-input
                    outlined
                    stack-label
                    readonly
                    label="API Hits"
                    :value="apiKey.hits"
                  />
                </div>
                <div class="col-4 q-px-xs">
                  <q-input
                    ref="limit"
                    outlined
                    readonly
                    stack-label
                    label="API Hit Limit"
                    type="number"
                    :value="apiKey.limit"
                  />
                </div>
              </div>
              <div class="row">
                <ApiKeyScope readonly :scope="apiKey.payload.scope" />
              </div>
            </div>
          </div>
        </q-card-section>
        <q-separator />

        <q-card-section class="q-py-sm q-pr-sm q-pl-lg">
          <div class="row justify-between items-center">
            <div class="col-auto text-caption text-negative">
              <span v-if="errors.revoke">{{ errors.revoke }}</span>
            </div>
            <div class="col-auto">
              <q-btn flat @click="confirmRevokeKey">Revoke API Key</q-btn>
            </div>
          </div>
        </q-card-section>
      </div>
    </q-slide-transition>
  </q-card>
</template>

<script lang="ts">
import {
  computed,
  defineComponent,
  reactive,
  toRefs
} from "@vue/composition-api";
import { copyToClipboard, Dark, Dialog } from "quasar";
import ms from "ms";

import ApiKeyScope from "components/ApiKeyScope.vue";
import useNotify from "composables/use-notify";
import { ApiKey } from "../store/services/api-keys";

export default defineComponent<{
  apiKey: ApiKey;
}>({
  name: "CardApiKey",
  components: { ApiKeyScope },
  props: {
    apiKey: {
      type: ApiKey,
      default: null
    }
  },
  setup(props) {
    const notify = useNotify();

    const state = reactive({
      expanded: false,
      errors: {
        revoke: false
      }
    });

    const badgeColor = computed(() => {
      let ret;
      if (props.apiKey.expiresAt < new Date()) {
        ret = "red";
      } else {
        const pctUsed = props.apiKey.hits / props.apiKey.limit;
        if (pctUsed >= 1.0) {
          ret = "red";
        } else if (pctUsed >= 0.7) {
          ret = "orange";
        } else {
          ret = "green";
        }
      }
      if (Dark.isActive) {
        ret = `keep-${ret}`;
      }
      return ret;
    });

    const badgeText = computed(() => {
      if (props.apiKey.expiresAt < new Date()) {
        return "expired";
      }
      if (props.apiKey.hits >= props.apiKey.limit) {
        return "used up";
      }
      return `${props.apiKey.hits.toLocaleString()} / ${props.apiKey.limit.toLocaleString()}`;
    });

    const expiresIn = computed(() => {
      return ms(props.apiKey.expiresAt.getTime() - Date.now(), { long: true });
    });

    const prettyPayload = computed(() => {
      return JSON.stringify(props.apiKey.payload, null, "  ");
    });

    function copyApiKey() {
      copyToClipboard(props.apiKey.accessToken);
      notify.create({ message: "Copied API key to clipboard" });
    }

    async function revokeKey() {
      state.errors.revoke = false;
      const desc = props.apiKey.description
        ? props.apiKey.description
        : props.apiKey.id;
      try {
        await props.apiKey.remove();
      } catch (e) {
        state.errors.revoke = e;
        return;
      }
      notify.create({
        message: `Revoked API key "${desc}"`,
        icon: "vpn_key"
      });
    }

    function confirmRevokeKey() {
      const desc = props.apiKey.description
        ? props.apiKey.description
        : props.apiKey.id;
      Dialog.create({
        title: "Confirm",
        message: `Revoke API key "${desc}"?`,
        cancel: true
      }).onOk(revokeKey);
    }

    return {
      // State
      ...toRefs(state),
      // Computes
      badgeColor,
      badgeText,
      expiresIn,
      prettyPayload,
      // Methods
      confirmRevokeKey,
      copyApiKey
    };
  }
});
</script>

<style>
</style>