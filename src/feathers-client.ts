// src/feathers-client.js
import auth, { Storage as IStorage } from "@feathersjs/authentication-client";
import feathers, { Service } from "@feathersjs/feathers";
import socketio from "@feathersjs/socketio-client";
import fastCopy from "fast-copy"
import { discard } from "feathers-hooks-common";
import feathersVuex, { ServiceState } from "feathers-vuex";
import { debounce, uid } from "quasar"
import io from "socket.io-client";
import Vue from "vue"
import { StoreOptions } from "vuex/types";

import { ServiceEventWithContext } from "src-srv/channels";
import { IdType, DataModel } from "src-srv/types";

import { Plugins } from "app/src-capacitor/node_modules/@capacitor/core";
import { Paginated as FVPaginated, Params as FVParams, Query as FVQuery } from "feathers-vuex/dist/utils";
import { ModelInstanceOptions } from "feathers-vuex/dist/service-module/types";
const { Storage: StorageApi } = Plugins;

class CapacitorStorage implements IStorage {
  getItem(key: string) {
    return StorageApi.get({ key }).then(({ value }) => value);
  }

  setItem(key: string, value: unknown) {
    if (typeof value === "string") {
      return StorageApi.set({ key, value });
    } else {
      return StorageApi.set({ key, value: `${value}` });
    }
  }

  removeItem(key: string) {
    return StorageApi.remove({ key });
  }
}

const appStorage = new CapacitorStorage();
export async function getDeviceUid() {
  let deviceUid = await appStorage.getItem("deviceUid");
  if (!deviceUid) {
    deviceUid = uid();
    await appStorage.setItem("deviceUid", deviceUid)
  }
  return deviceUid;
}
// Populate deviceUid
getDeviceUid();

/** See https://quasar.dev/quasar-cli/cli-documentation/handling-process-env#Stripping-out-code */
let domain;
if (process.env.DEV) {
  const loc = window.location;
  domain = `${loc.protocol}//${loc.hostname}:3030`;
} else {
  domain = window.location.origin;
}
const socket = io(domain, { transports: ["websocket"] });

const discardFeathersVuexTempFields = discard("__id", "__isTemp");
const feathersClient = feathers()
  .configure(socketio(socket))
  .configure(auth({ storage: appStorage }))
  .hooks({
    before: {
      create: [discardFeathersVuexTempFields],
      update: [discardFeathersVuexTempFields],
      patch: [discardFeathersVuexTempFields]
    }
  });

export default feathersClient;

// Setting up feathers-vuex
const {
  makeServicePlugin,
  makeAuthPlugin,
  BaseModel,
  models,
  FeathersVuex
} = feathersVuex(feathersClient, {
  serverAlias: "api", // optional for working with multiple APIs (this is the default value)
  idField: "id", // Must match the id field in your database table/collection
  whitelist: ["$regex", "$options"],
  handleEvents: {
    created: (event: ServiceEventWithContext) => [true, event.data],
    updated: (event: ServiceEventWithContext) => [true, event.data],
    patched: (event: ServiceEventWithContext) => [true, event.data],
    removed: (event: ServiceEventWithContext) => [true, event.data],
  }
});

interface PaginationState {
  ids: number[]
  limit: number
  skip: number
  ip: number
  total: number
  mostRecent: {
    query: {},
    queryId: string,
    queryParams: FVParams,
    pageId: string,
    pageParams: {
      $limit: number, $skip: number
    },
    queriedAt: number
  }
}

interface AutoSaveExtensionOptions {
  service: Service<unknown>;
  Model: unknown;
}

interface AutoSaveServiceState {
  /** Track IDs that are currently debouncing a call to save() */
  saveDebouncingById: { [key in IdType]: boolean }
  /** Track IDs that are currently saving */
  saveWaitingIds: IdType[]
}

function makeAutoSaveServiceExtension(
  options: AutoSaveExtensionOptions
): StoreOptions<AutoSaveServiceState> {

  const { service } = options;

  return {
    state: {
      saveDebouncingById: {},
      saveWaitingIds: []
    },
    getters: {
      getSaveDebouncing({ saveDebouncingById }) {
        for (const key in saveDebouncingById) {
          if (Object.prototype.hasOwnProperty.call(saveDebouncingById, key)) {
            return true;
          }
        }
        return false;
      },
      getSaveDebouncingById({ saveDebouncingById }) {
        return (id: IdType) => saveDebouncingById[id] === true;
      },
      getSavePending(_, { getSaveDebouncing, getSaveWaiting }): boolean {
        return getSaveDebouncing || getSaveWaiting;
      },
      getSaveWaiting({ saveWaitingIds }) {
        return saveWaitingIds.length > 0;
      },
      getSaveWaitingById({ saveWaitingIds }) {
        return (id: IdType) => saveWaitingIds.includes(id);
      },
      idExists(state) {
        const { keyedById, tempsById } = state as unknown as ServiceState; // Extract FeathersVuex state
        return (id: IdType) => !!keyedById[id] || !!tempsById[id]
      }
    },
    mutations: {
      setIdDebouncing({ saveDebouncingById }, id) {
        Vue.set(saveDebouncingById, id, true);
      },
      setIdSaving({ saveWaitingIds }, id) {
        saveWaitingIds.push(id);
      },
      unsetIdDebouncing({ saveDebouncingById }, id) {
        Vue.delete(saveDebouncingById, id);
      },
      unsetIdSaving({ saveWaitingIds }, id) {
        const idx = saveWaitingIds.indexOf(id);
        if (idx >= 0) {
          Vue.delete(saveWaitingIds, idx);
        }
      }
    },
    actions: {
      async create({ commit, dispatch, state }, dataOrArray) {
        const { idField, keyedById, tempIdField } = state as unknown as ServiceState;
        let data
        let params
        let tempIds
        let savingIds

        if (Array.isArray(dataOrArray)) {
          data = dataOrArray[0]
          params = dataOrArray[1]
        } else {
          data = dataOrArray
        }

        params = fastCopy(params)

        if (Array.isArray(data)) {
          savingIds = data.map(i => i[idField] || i[tempIdField]);
          tempIds = data.map(i => i[tempIdField])
        } else {
          savingIds = [data[idField] || data[tempIdField]];
          tempIds = [data[tempIdField]] // Array of tempIds
        }

        params = params || {}

        savingIds.forEach(id => commit("setIdSaving", id));
        commit("setPending", "create")

        let response;
        try {
          response = await service.create(data, params) as DataModel | DataModel[]
        } catch (error) {
          commit("setError", { method: "create", error })
          return error
        } finally {
          commit("unsetPending", "create");
          savingIds.forEach(id => commit("unsetIdSaving", id));
        }

        if (Array.isArray(response)) {
          await dispatch("addOrUpdateList", response);
          response = response.map(item => keyedById[item[idField] as IdType]);
        } else {
          const id = response[idField]
          const tempId = tempIds[0]

          if (id != null && tempId != null) {
            commit("updateTemp", { id, tempId })
          }
          response = await dispatch("addOrUpdate", response)
          // response = state.keyedById[id]
        }
        commit("removeTemps", tempIds)
        return response as DataModel
      },
      async patch({ commit, dispatch, state }, [id, data, params]) {
        const { keyedById } = state as unknown as ServiceState;
        // Set status
        commit("setIdSaving", id);
        commit("setPending", "patch")

        // Normal FeathersVuex behavior
        params = fastCopy(params)
        if (service.FeathersVuexModel && params && !params.data) {
          data = service.FeathersVuexModel.diffOnPatch(data)
        }
        if (params && params.data) {
          data = params.data
        }

        // Call to API server
        let item;
        try {
          item = await service.patch(id, data, params)
        } catch (error) {
          commit("setError", { method: "patch", error })
          return error
        } finally {
          commit("unsetPending", "patch");
          commit("unsetIdSaving", id);
        }

        await dispatch("addOrUpdate", item)
        return keyedById[id];
      },
    }
  }
}

export interface AutoSaveBaseModelInstanceOptions extends ModelInstanceOptions {
  debounce?: number;
}

export type Paginated<T> = FVPaginated<T>

export type SortOrder = 1 | -1;

export interface Query extends FVQuery {
  $limit?: number;
  $skip?: number;
  $sort?: { [key: string]: SortOrder }
}

export interface Params extends FVParams {
  temps?: boolean;
  qid?: string;
  query: Query
}

declare module "feathers-vuex" {
  interface ModelStatic {
    _getters: any
    _commit: any
  }
}

/**
 * A Feathers-Vuex base model that eases auto-save functionality
 */
class AutoSaveBaseModel extends BaseModel {

  constructor(data?: any, options?: AutoSaveBaseModelInstanceOptions) {
    super(data, options);

    if (!(this._saveDebounce as ReturnType<typeof debounce>).cancel) {
      const debounceTime = (options && options.debounce) || 500;
      this._saveDebounce = debounce(this._saveDebounce.bind(this), debounceTime)
    }
  }

  /**
   * Copy Model fields from `a` to `b`
   * @param a source Model
   * @param b dest Model
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  static copy(a: Partial<AutoSaveBaseModel>, b: Partial<AutoSaveBaseModel>) {
    throw new Error(`${this.constructor.name} must implement "copy()"`);
  }

  /** Debounce calls to save() to prevent overloading the API server
   * @param params Parameters to pass to API server
  */
  _saveDebounce(params?: FVParams) {
    const { _commit } = this.constructor as typeof BaseModel;
    _commit.call(this.constructor, "unsetIdDebouncing", this.__id || this.id);
    // If item was removed while debouncing, do not call save
    if (this.existsInStore) {
      this.save(params)
    }
  }

  /**
   * Copy fields from `other` to `this`
   * @param other source Model
   */
  copyFrom(other: Partial<this>) {
    (this.constructor as typeof AutoSaveBaseModel).copy(other, this);
    return this;
  }

  /**
   * Copy fields from `this` to `other`
   * @param other destination Model
   */
  copyTo(other: Partial<this>) {
    (this.constructor as typeof AutoSaveBaseModel).copy(this, other);
    return other;
  }

  /**
   * Create this instance on the API server
   * @param params params for server
   */
  async create(params?: FVParams) {
    const instance = await super.create(params);
    (this.constructor as typeof BaseModel).emit("after-create", instance);
    return instance;
  }

  /**
   * Remove this instance from the API server
   * @param params params for server
   */
  async remove(params?: FVParams): Promise<this> {
    const instance = await super.remove(params);
    (this.constructor as typeof BaseModel).emit("after-remove", instance);
    return instance;
  }

  /**
   * Save `this` model to API server
   * @param params Parameters to pass to API server
   */
  save(params?: FVParams) {
    // Prevent multiple 'create' calls on temps
    if (this.__isTemp && this.isSaveWaiting) {
      this.saveDebounce(params);
      return Promise.resolve(this);
    }
    return super.save(params);
  }

  /**
   * Debounce calls to save() to prevent overloading the API server
   * @param params Parameters to pass to API server
   */
  saveDebounce(params?: FVParams) {
    const { _commit } = this.constructor as typeof BaseModel;
    _commit.call(this.constructor, "setIdDebouncing", this.__id || this.id);
    this._saveDebounce(params);
  }

  /**
   * Update properties on `this` Model
   * 
   * Makes use of Feathers-Vuex's clone/commit pattern to avoid mutating Vuex
   * state outside of the store. 
   * @param props 
   * @param eager call `commit` before `save` to update the UI?
   * @param debounce debounce the call to `save`?
   * @returns if `debounce` == false, returns the result of call to `save`.
   *          Otherwise, returns the cloned instance in a resolved Promise
   */
  setProps(
    props: Partial<this>,
    eager = true,
    debounce = true
  ) {
    const { _commit, copiesById } = this.constructor as typeof BaseModel;
    const id = this.id || this.__id;

    // Add to store now if not already
    if (!this.existsInStore) {
      _commit.call(this.constructor, "addItem", this)
    }

    // Obtain clone of this Model instance
    let clone: this | undefined = undefined;
    if (this.isSavePending) {
      clone = this.__isClone ? this : copiesById[id] as this;
    }
    if (clone) {
      // Assign new props
      Object.assign(clone, props);
    } else {
      // Assign new props during clone
      clone = this.clone(props);
    }

    // Eager updates
    if (eager) {
      clone.commit();
    }

    // Save now or in a bit, but ignore response from server
    const params = { pruneApiResponse: true };
    if (debounce) {
      clone.saveDebounce(params);
      return Promise.resolve(clone);
    } else {
      return clone.save(params);
    }
  }

  /**
   * Check if this model exists in the Vuex store
   */
  get existsInStore() {
    const { _getters } = this.constructor as typeof BaseModel;
    // prioritize real ID because FeathersVuex will no longer reference by tempId once real exists
    return _getters.call(this.constructor, "idExists", this.id || this.__id)
  }

  /**
   * Is this Model currently debouncing a call to save?
   */
  get isSaveDebouncing() {
    const { _getters } = this.constructor as typeof BaseModel;
    return _getters.call(this.constructor, "getSaveDebouncingById", this.__id || this.id)
  }

  /**
   * Is this Model currently debouncing OR waiting for save response?
   */
  get isSavePending() {
    return this.isSaveDebouncing || this.isSaveWaiting;
  }

  /**
   * Is this Model currently being saved and waiting for API response?
   */
  get isSaveWaiting() {
    const { _getters } = this.constructor as typeof BaseModel;
    return _getters.call(this.constructor, "getSaveWaitingById", this.__id || this.id)
  }

} // AutoSaveBaseModel

export { makeAuthPlugin, makeServicePlugin, BaseModel, models, FeathersVuex, AutoSaveBaseModel, makeAutoSaveServiceExtension };
