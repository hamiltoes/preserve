import { Platform } from "quasar";

/**
 * Obtain a descriptive name from device details
 * @param details Device details from Quasar's Platform.is
 */
export function getDeviceName(details?: Platform["is"]) {
    let name: string | undefined;
    if (details) {
        if (details.mobile) {
            if (details.android) {
                name = "Android device";
            } else if (details.iphone) {
                name = "iPhone";
            } else if (details.ipad) {
                name = "iPad";
            } else if (details.ipod) {
                name = "iPod";
            } else if (details.ios) {
                name = "iOS device";
            } else {
                name = "mobile device";
            }
        } else if (details.desktop) {
            let browser
            if (details.chrome) {
                browser = "Chrome";
            } else if (details.edge) {
                browser = "Edge";
            } else if (details.firefox) {
                browser = "Firefox";
            } else if (details.opera) {
                browser = "Opera";
            } else if (details.safari) {
                browser = "Safari"
            }
            const browserStr = browser ? `${browser} on ` : ""
            if (details.win) {
                name = `${browserStr}Windows`
            } else if (details.mac) {
                name = `${browserStr}Mac`
            } else if (details.linux) {
                name = `${browserStr}Linux`
            } else if (details.cros) {
                name = "Chromebook"
            }
        }
    }
    if (!name) {
        name = "another device";
    }
    return name;
}