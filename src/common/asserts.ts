/**
 * Assertions
 */

/**
 * Assert value is `boolean`
 * @param val expected boolean
 */
export function assertBool(val: unknown): asserts val is boolean {
    const t = typeof val;
    if (t !== "boolean") {
        throw new Error(`Expected 'val' to be boolean, but received '${t}'`);
    }
}

/**
 * Assert value is not `null` or `undefined`
 * @param val value to check for existence
 */
export function assertExists<T>(val: T): asserts val is NonNullable<T> {
    if (val === undefined || val === null) {
        throw new Error(`Expected 'val' to be defined, but received ${val}`);
    }
}
