import { computed, reactive, Ref, ref, watch } from "@vue/composition-api";
import Vue from "vue";

import useEventBus from "./use-event-bus";
import { Paginated, SortOrder } from "../feathers-client";
import { Note } from "../store/services/notes";

const { ListEntry } = Vue.$FeathersVuex.api;
type ListEntry = (typeof ListEntry)["prototype"]

interface UseListOptions {
    /** The note  */
    note: Readonly<Ref<Readonly<Note>>>,
    /** Preview mode */
    preview?: Readonly<Ref<Readonly<boolean>>>,
    /** Include temp entries */
    temps?: boolean,
    /** This instance is responsible for talking to remote server */
    handleQueryingRemote?: boolean,
    /** Query during setup? (Only has an effect if handleQueryingRemote === true) */
    lazy?: boolean
}

interface LastResponseType extends Paginated<ListEntry> {
    receivedAt: number
}

interface UseListNoteState {
    isLoading: boolean,
    lastModified?: number,
    lastResponse?: LastResponseType,
    previewLimit: number,
    $limit: number,
    $skip: number,
}

const notesState: { [key: number]: UseListNoteState } = reactive({});
function getState(id: number) {
    if (!notesState[id]) {
        Vue.set(notesState, id, reactive({
            isLoading: false,
            lastModified: undefined,
            lastResponse: undefined,
            previewLimit: 10,
            $limit: 50,
            $skip: 0,
        }))
    }
    return notesState[id];
}

/**
 * Use list composable
 * @param {Note} note Note who's list we're using
 * @param {boolean} temps find temps in store?
 */
export default function useList({
    note,
    preview = ref(false),
    temps = true,
    handleQueryingRemote = false,
    lazy = false
}: UseListOptions) {

    /** Get state for this note and react to note changes */
    const state = ref(getState(note.value.id));
    watch(
        () => note.value.id,
        id => state.value = getState(id),
        { immediate: false }
    );

    /** Unique QID for this note */
    const qid = computed(() => `note-${note.value.id}-list`);

    /** Find list items locally params */
    const localParams = computed(() => {
        return {
            query: {
                noteId: note.value.id,
                $sort: { done: 1 as SortOrder },
                ...(preview.value && { $limit: state.value.previewLimit })
            },
            temps,
        };
    });

    /** Find list items on API server params */
    const fetchParams = computed(() => {
        return {
            qid: qid.value,
            query: {
                noteId: note.value.id,
                userId: note.value.userId,
                // Limit fetch to `previewLimit` in preview
                $limit: preview.value ? state.value.previewLimit : state.value.$limit,
                $skip: state.value.$skip,
                $sort: { done: 1 as SortOrder }
            }
        };
    });

    /** Result from local query to store */
    const foundInStoreResult = computed(() => {
        return ListEntry.findInStore<ListEntry>(localParams.value)
    });

    /** Items from local query result */
    const itemsAll = computed(() => {
        return foundInStoreResult.value.data
    })

    /** Total number of items in this list */
    const itemsTotal = computed(() => {
        const lr = state.value.lastResponse;
        if (lr) {
            // If a local change (add/remove) has happened since
            // the last query, use the local pagination's total
            if (state.value.lastModified && lr.receivedAt < state.value.lastModified) {
                return foundInStoreResult.value.total;
            }
            // Otherwise use the max of the fetch response and the local total
            return Math.max(lr.total, foundInStoreResult.value.total)
        }
    })

    /** Has loaded all items from the server for this list */
    const finishedLoading = computed(() => {
        const lr = state.value.lastResponse;
        if (!lr) {
            return false;
        }
        // This scenario catches when a duplicate is created and
        // the initial query for it's items returns zero because
        // the items are still being created
        if (lr.total === 0 && foundInStoreResult.value.total > 0) {
            return false;
        }
        const queriedMax = lr.skip + lr.limit;
        return queriedMax >= lr.total;
    })

    /** List done index */
    const doneIndex = computed(() => {
        const idx = itemsAll.value.findIndex(e => e.done);
        if (idx === -1) {
            return itemsAll.value.length;
        }
        return idx;
    })

    /** Done items */
    const itemsDone = computed(() => itemsAll.value.slice(doneIndex.value));
    /** Todo items */
    const itemsTodo = computed(() => itemsAll.value.slice(0, doneIndex.value));

    /** Done items count (can't just use itemsDone.length
     * because entire list might not have loaded yet)
     */
    const itemsDoneCount = computed(() => {
        if (finishedLoading.value) {
            return itemsDone.value.length;
        } else if (itemsTotal.value) {
            return itemsTotal.value - itemsTodo.value.length;
        }
    })

    /**
     * Delete items from API server
     * @param items items to delete
     * @param eager eager removal from Vuex
     */
    function deleteItems(items: readonly ListEntry[], eager = true): Promise<readonly ListEntry[]> {
        return Promise.all(items.map(i => i.remove({ eager })));
    }

    /**
     * Delete all items from API server
     * @param {boolean} eager eager removal from Vuex
     */
    function deleteAllItems(eager = true) {
        return deleteItems(itemsAll.value, eager);
    }

    /**
     * Delete all done items from API server
     * @param {boolean} eager eager removal from Vuex
     */
    function deleteDoneItems(eager = true) {
        return deleteItems(itemsDone.value, eager);
    }

    /**
     * Convert list items to text
     * @param {ListEntry[]} items items to convert to text
     * @param {boolean} textChecks include text checkboxes? (e.g. [ ], [x])
     * @returns {string}
     */
    function itemsAsText(items: readonly ListEntry[], textChecks = false) {
        const finalText = [];
        for (const e of items) {
            let text = e.text.trim();
            if (text !== "") {
                if (textChecks) {
                    text = `[${e.done ? "x" : " "}] ${text}`;
                }
                finalText.push(text);
            }
        }
        return finalText.join("\n");
    }

    /** Load next page of items from server */
    async function loadNextPage() {
        if (finishedLoading.value) { return true; }
        state.value.isLoading = true;
        let resp: Paginated<ListEntry>;
        try {
            resp = await ListEntry.find<ListEntry>(fetchParams.value) as Paginated<ListEntry>;
        } finally {
            state.value.isLoading = false;
        }
        const respWithTimestamp: LastResponseType = resp as LastResponseType;
        respWithTimestamp.receivedAt = Date.now();
        Vue.set(state.value, "lastResponse", resp);
        state.value.$skip += fetchParams.value.query.$limit;
        return finishedLoading.value;
    }

    /** Load all items from server */
    async function loadAllItems() {
        while (!finishedLoading.value) {
            await loadNextPage();
        }
        return itemsAll.value;
    }

    /** Uncheck all `done` items */
    function uncheckAll() {
        for (const item of itemsDone.value) {
            item.setProps({ done: false }, true, false);
        }
    }

    // If this instance is handling querying the API server,
    // we need to watch some events and load the first page
    if (handleQueryingRemote === true) {

        // Watch for local create/remove since and save timestamp
        useEventBus(ListEntry, ["after-create", "after-remove"], (e: ListEntry) => {
            if (e.noteId == note.value.id) {
                state.value.lastModified = Date.now();
            }
        });

        // Kick off initial query if not lazy
        if (!lazy) {
            loadNextPage();
        }
    }

    return {
        // Computes
        doneIndex,
        itemsAll,
        itemsDone,
        itemsDoneCount,
        itemsFinishedLoading: finishedLoading,
        itemsLoading: computed(() => state.value.isLoading),
        itemsPagination: computed(() => state.value.lastResponse),
        itemsPerPage: computed(() => state.value.previewLimit),
        itemsTodo,
        itemsTotal,
        // Methods
        deleteAllItems,
        deleteDoneItems,
        itemsAsText,
        loadAllItems,
        loadNextPage,
        uncheckAll,
    }
}