import { computed, ref } from "@vue/composition-api";
import { Plugins, GeolocationOptions, GeolocationPosition } from "app/src-capacitor/node_modules/@capacitor/core";

const { Geolocation } = Plugins;

export type GeolocationCoords = GeolocationPosition["coords"];

/**
 * Use Capacitor's Geolocation API
 */
export default function useGeolocation() {

    const _obtaining = ref(false);

    return {
        /** GPS location is currently being obtained */
        obtaining: computed(() => _obtaining.value),

        /**
         * Get the current GPS location of the device
         * @param options fdfsafd
         */
        getPosition: async (options?: GeolocationOptions): Promise<GeolocationPosition> => {
            _obtaining.value = true;
            try {
                return await Geolocation.getCurrentPosition(options);
            } finally {
                _obtaining.value = false;
            }
        }
    }
}
