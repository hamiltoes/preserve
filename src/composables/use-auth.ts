import { FeathersError } from "@feathersjs/errors";
import { computed, Ref, reactive } from "@vue/composition-api";
import { Platform } from "quasar";
import Vue from "vue";

import useNotify from "./use-notify";
import feathersClient, { getDeviceUid } from "src/feathers-client";
import store from "src/store";
import { DeviceDetails } from "src-srv/authentication";
import { isApiKeyPayload } from "src-srv/services/api-key/api-key.class";

const { User } = Vue.$FeathersVuex.api;

interface LocalStrategy {
    strategy: "local",
    email: string,
    password: string
}

interface JwtStrategy {
    strategy: "jwt",
    accessToken: string
}

type AuthStrategy = JwtStrategy | LocalStrategy;

interface UseAuthOptions {
    fields?: Ref<{ validate(): boolean | Promise<boolean> } | null>[];
}

export default function useAuth(opts?: UseAuthOptions) {
    const { fields = [] } = opts || {};

    const notify = useNotify();

    const state = reactive({
        isCreateUserPending: false,
        loginError: null as FeathersError | null,
        logoutError: null as FeathersError | null,
        signupError: null as FeathersError | null
    });

    const payload = computed(() => store.state.auth.payload);
    const isApiKey = computed(() => isApiKeyPayload(payload.value));

    /**
     * Attempt authentication
     * @param credentials Auth credentials
     * @param redirect Redirect on success?
     */
    async function login(credentials: AuthStrategy, redirect = true) {
        try {
            const deviceDetails: DeviceDetails = {
                uid: await getDeviceUid(),
                details: Platform.is
            }
            await store.dispatch("auth/authenticate", [credentials, { deviceDetails }]);
            if (redirect) {
                const q = store.$router.currentRoute.query;
                const next =
                    typeof q.redirect === "string" ? q.redirect : { name: "Home" };
                store.$router.push(next);
            }
        } catch (error) {
            if (error.code === 401) {
                notify.action(error.message)
            }
            throw error;
        }
    }

    /** Logout user */
    async function logout() {
        state.logoutError = null;
        try {
            await store.dispatch("auth/logout");
            store.$router.push({ name: "Login" });
            // Refresh to clear data from Vuex
            window.location.reload();
        } catch (error) {
            state.logoutError = error;
            if (error.message) {
                notify.action(error.message);
            }
        }
    }

    /**
     * Attempt to re-authenticate using the stored JWT
     * @param redirect Redirect on success?
     * @param ignoreFailure throw error on failed auth?
     */
    async function reAuthenticate(redirect = false, ignoreFailure = false) {
        const accessToken = await feathersClient.authentication.getAccessToken();
        if (!accessToken) {
            if (ignoreFailure) {
                return;
            }
            throw new Error("No accessToken found in storage");
        }
        return await login({ strategy: "jwt", accessToken }, redirect)
    }

    /** Validate all fields */
    function validate() {
        return fields.every(f => {
            if (f.value) {
                return f.value.validate()
            } else {
                return false;
            }
        });
    }

    /**
     * Validate fields then attempt to login if valid
     * @param credentials auth credentials
     */
    async function validateAndLogin(credentials: AuthStrategy) {
        state.loginError = null;
        if (!validate()) {
            notify.action("Please correct errors");
            return;
        }
        try {
            await login(credentials);
        } catch (error) {
            state.loginError = error;
        }
    }

    /**
     * Validate fields then attempt to signup and login if valid
     * @param user new user credentials
     */
    async function validateAndSignup(user: LocalStrategy) {
        state.signupError = null;
        if (!validate()) {
            notify.action("Please correct errors");
            return;
        }
        state.isCreateUserPending = true;
        const u = new User(user);
        try {
            await u.create();
        } catch (error) {
            console.error(error);
            if (error.code === 409) {
                state.signupError = error;
            }
            notify.action("Failed to create user");
            return;
        } finally {
            state.isCreateUserPending = false;
        }
        return await login(user);
    }

    return {
        // Computes
        /** Payload is an API key payload */
        isApiKey,
        /** Is authentication pending? */
        isAuthPending: computed(() => store.state.auth.isAuthenticatePending),
        /** Is create user pending? */
        isCreateUserPending: computed(() => state.isCreateUserPending),
        /** Is logout pending? */
        isLogoutPending: computed(() => store.state.auth.isLogoutPending),
        /** Error received during login */
        loginError: computed(() => state.loginError),
        /** Error received during logout */
        logoutError: computed(() => state.logoutError),
        /** Authentication (JWT) payload */
        payload,
        /** Error received during signup */
        signupError: computed(() => state.signupError),
        // Methods
        login,
        logout,
        reAuthenticate,
        validateAndLogin,
        validateAndSignup,
        // Composables
        notify
    }
}