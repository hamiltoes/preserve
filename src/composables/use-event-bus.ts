import { onMounted, onUnmounted } from "@vue/composition-api";

interface EventBus {
    on(eventName: string, handler: Function): unknown
    off(eventName: string, handler: Function): unknown
}

/**
 * Registers an event handler on mount and unregisters on unmount
 * @param bus event bus
 * @param event event name
 * @param handler event handler
 */
export default function useEventBus(bus: EventBus, event: string | string[], handler: Function) {
    const events = Array.isArray(event) ? event : [event];
    events.forEach(name => {
        onMounted(() => bus.on(name, handler));
        onUnmounted(() => bus.off(name, handler));
    });
}
