import { computed } from "@vue/composition-api";
import { Dark, Notify, Screen } from "quasar";

export type Undoable = (undone: boolean) => unknown;

type CreateOptions = Parameters<Notify["create"]>[0]

export default function useNotify() {
    const defaultConfig = computed(() => ({
        position: Screen.xs ? <const>"bottom" : <const>"bottom-left",
        progress: true,
        color: Dark.isActive ? "keep-darkblue" : "primary",
        textColor: "color-body-dark"
    }));

    const defaultActionConfig = {
        color: "color-body-dark",
    }

    function create(opts: CreateOptions) {
        if (typeof opts === "string") {
            opts = { message: opts }
        }
        return Notify.create({
            ...defaultConfig.value,
            ...opts
        });
    }

    function actionUndone() {
        create({
            message: "Action undone",
            actions: [
                {
                    ...defaultActionConfig,
                    dense: true,
                    icon: "close",
                    round: true
                }
            ]
        });
    }

    function action(message: string, undoer?: Undoable) {
        create({
            message,
            multiLine: false,
            actions: [
                ...(undoer ? [{
                    ...defaultActionConfig,
                    label: "Undo",
                    handler() {
                        undoer(true);
                        actionUndone();
                    }
                }] : []),
                {
                    ...defaultActionConfig,
                    dense: true,
                    icon: "close",
                    round: true
                }
            ]
        });
    }

    return {
        action,
        actionUndone,
        create
    }
}
