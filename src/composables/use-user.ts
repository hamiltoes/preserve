import { computed } from "@vue/composition-api";

import useNotify from "./use-notify";
import useGeolocation from "./use-geolocation";
import store from "src/store";
import { User } from "src/store/services/users";

/**
 * Use user composable
 */
export default function useUser() {

    const geo = useGeolocation();
    const notify = useNotify();

    const user = computed(() => store.getters["auth/user"] as User | undefined);
    const userId = computed(() => user.value ? user.value.id : NaN);
    const settings = computed(() => (user.value && user.value.settings))

    /** Toggle between list and grid display */
    function toggleDisplay() {
        const s = settings.value;
        s?.setProps({ displayGrid: !s.displayGrid });
    }

    /** Toggle ability to expand checklist in preview */
    function toggleExpandListInPreview() {
        const s = settings.value;
        s?.setProps({ allowExpandChecklistInPreview: !s.allowExpandChecklistInPreview });
    }

    /** Toggle ability to geotag notes */
    async function toggleGeotagNotes() {
        const s = settings.value;
        if (!s) return;
        const geotagNotes = !s.geotagNotes;
        if (geotagNotes) {
            try {
                // Obtain position to check permission
                await geo.getPosition();
            } catch {
                notify.create({
                    color: "negative",
                    message: "Failed to get location permission"
                });
                return;
            }
        }
        s.setProps({ geotagNotes });
    }

    /** Toggle between dark and light theme */
    function toggleTheme() {
        const s = settings.value;
        s?.setProps({ darkTheme: !s.darkTheme });
    }

    return {
        // Computes
        settings,
        user,
        userId,

        // Methods
        toggleDisplay,
        toggleExpandListInPreview,
        toggleGeotagNotes,
        toggleTheme,

        // useGeolocation
        obtainingPosition: geo.obtaining
    }

}