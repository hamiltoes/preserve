import { computed, reactive, Ref, ref } from "@vue/composition-api";
import { Dialog, Platform, copyToClipboard, extend, openURL, LooseDictionary, Screen, debounce } from "quasar";
import Vue from "vue";
import { Plugins } from "app/src-capacitor/node_modules/@capacitor/core";

import useEventBus from "./use-event-bus";
import useDelay from "./use-delay";
import useGeolocation, { GeolocationCoords } from "./use-geolocation";
import useList from "./use-list";
import useNotify from "./use-notify";
import { assertExists } from "common/asserts";
import { getDeviceName } from "common/quasar-help";
import NoteToChecklistDialog from "components/NoteToChecklistDialog.vue";
import { getDeviceUid } from "src/feathers-client";
import store from "src/store"
import { ServiceEventWithContext } from "src-srv/channels";

const { Share } = Plugins;
const { ListEntry, Note } = Vue.$FeathersVuex.api;
type Note = typeof Note["prototype"]

const inUseTimeout = 3000;

function toDateDisplayString(date?: Date) {
    if (!date) return "";
    return date.toLocaleString(undefined, {
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric"
    });
}

interface UseNoteOptions {
    /** note to use */
    note: Readonly<Ref<Readonly<Note>>>;
    /** using component is in preview? */
    preview: Readonly<Ref<Readonly<boolean>>>;
    /** Watch and notify upon service events from other devices */
    watchServiceEvents?: boolean;
}

/**
 * Use note composable
 */
export default function useNote({ note, preview = ref(false), watchServiceEvents = false }: UseNoteOptions) {

    const geo = useGeolocation();
    const list = useList({ note, preview });
    const notify = useNotify();
    const delay = computed(() => (preview.value ? 0 : 500));

    const state = reactive({
        foreignDeviceUsing: false
    })

    /**
     * Toggle note's `archived` status
     * @param undone true if function call is undoing `archive()`
     */
    function archive(undone = false) {
        const archived = !note.value.archived;
        note.value.setProps({ archived });
        if (undone === false) {
            notify.action(
                `Note ${archived ? "" : "un"}archived`,
                archive
            );
        }
    }

    /** Clear inUseByForeignDevice status */
    const clearBeingUsedByForeignDevice = debounce(() => {
        state.foreignDeviceUsing = false
    }, inUseTimeout)

    /**
     * Convert list to note
     * @param deleteChecked discard checked items?
     */
    async function convertListToNote(deleteChecked: boolean) {
        if (!note.value.checkboxes) { return; }
        const items = deleteChecked ? list.itemsTodo.value : list.itemsAll.value;
        const text = list.itemsAsText(items);
        // Set note body and hide checkboxes
        await note.value.setProps({ text, checkboxes: false }, true, false);
        // Wait for removal on API server
        await list.deleteAllItems();
    }

    /** Convert note to list */
    async function convertNoteToList() {
        if (note.value.checkboxes) { return; }
        // Split at newlines and discard empty lines
        const lines = note.value.text
            .split(/\r\n|\r|\n/gm)
            .map(l => l.trim())
            .filter(l => l !== "");
        // Create ListEntry models
        const items = lines.map(
            text =>
                new ListEntry({ noteId: note.value.id, text, userId: note.value.userId })
        );
        // Reset note body and show checkboxes
        await note.value.setProps({ text: "", checkboxes: true }, true, false);
        // Create new list-entries
        await store.dispatch("list-entries/create", [items]);
    }

    /**
     * Delete note from API server
     * @param {boolean} eager eager removal from Vuex store
     */
    function deleteNote(eager = true) {
        return note.value.remove({ eager });
    }

    /**
     * Handle an internal service event
     * @param e service event with context
     * @returns event has been handled, do nothing else
     */
    async function handleInternalEvent(e: ServiceEventWithContext): Promise<boolean> {
        const ctx = e.$context;
        assertExists(ctx.internal);
        const myUid = await getDeviceUid()
        if (ctx.internal.kind === "hook") {
            switch (ctx.internal.hook) {
                case "set-foreign-with-local": {
                    return ctx.device?.uid === myUid // ignore this event if it's from myself
                }
            }
        }
        else {
            switch (ctx.internal.chore) {
                case "empty-trash": {
                    notify.create({
                        badgeClass: "hidden",
                        message: "Note decayed to dust in the trash",
                        progress: false
                    });
                    return true // ignore this event
                }
            }
        }
    }

    /**
     * Handle an incoming service event
     * @param e service event with context
     * @param removed event is a `removed` event
     */
    async function handleEvent(e: ServiceEventWithContext, removed = false) {
        if (!preview.value && e.data.id === note.value.id) {
            const ctx = e.$context;
            if (ctx.internal) {
                if (await handleInternalEvent(e)) {
                    return;
                }
            }
            if (ctx.device) {
                const name = getDeviceName(ctx.device.details)
                if (removed) {
                    notify.create({
                        badgeClass: "hidden",
                        message: `Note was deleted by ${name}`,
                        progress: false
                    });
                } else {
                    const preposition = ctx.device.details.desktop ? "in" : "on"
                    state.foreignDeviceUsing = true;
                    notify.create({
                        badgeClass: "hidden",
                        message: `Note is being edited ${preposition} ${name}`,
                        progress: false,
                        timeout: inUseTimeout,
                        ...(Screen.xs && { position: "top" }),
                    });
                    clearBeingUsedByForeignDevice()
                }
            }
        }
    }

    /** Open note's location in external app */
    function openLocation() {
        const { latitude, longitude } = note.value.coords;
        const { title } = note.value;
        if (Platform.is.android) {
            openURL(`geo:0,0?q=${latitude},${longitude}(${title})`);
        } else if (Platform.is.ios) {
            // Not sure if this works
            openURL(`http://maps.apple.com/?ll=${latitude},${longitude}`);
        } else {
            openURL(
                `http://www.openstreetmap.org/?mlat=${latitude}&mlon=${longitude}&zoom=14`
            );
        }
    }

    /** Delete note after prompting user */
    function promptDelete() {
        Dialog.create({
            message: "Delete note forever?",
            cancel: true
        }).onOk(() => {
            Note.emit("before-remove", note);
            setTimeout(() => { deleteNote() }, delay.value);
        });
    }

    /** Duplicate the note */
    async function duplicate() {
        // Copy note and kick off save
        const notePromise = new Note(note.value.copyTo({})).save();

        // Don't need to wait around if there's no checklist items to copy
        if (!note.value.checkboxes || !list.itemsAll.value.length) {
            return await notePromise;
        }

        // Pull all items to duplicate
        await list.loadAllItems();

        // Prepare checklist items
        const itemsProps = list.itemsAll.value.map(i => i.copyTo({}));

        // Create new list-entries with new Note's ID
        const dupe = await notePromise;
        const items = itemsProps.map(i => new ListEntry({ noteId: dupe.id, ...i }));
        await store.dispatch("list-entries/create", [items]);
        return dupe;
    }

    /**
     * Restore note from trash
     * @param {boolean} undone true if function call is undoing `trash()`
     */
    function restore(undone = false) {
        if (note.value.trashed === 0) {
            return;
        }
        note.value.setProps({ trashed: 0 });
        if (undone === false) {
            //eslint-disable-next-line @typescript-eslint/no-use-before-define
            notify.action("Note restored", trash);
        }
    }

    /** Share note with device's Share API */
    async function share() {
        const text = note.value.checkboxes
            ? list.itemsAsText(list.itemsAll.value, true)
            : note.value.text;
        try {
            await Share.share({
                dialogTitle: "Share note",
                title: note.value.title,
                text
            })
        } catch {
            // no Share API
            copyToClipboard(text);
            notify.create({ message: "Copied note to clipboard" });
        }
    }

    /** Toggle between list and note */
    function toggleList() {
        // Convert to checklist
        if (!note.value.checkboxes) {
            convertNoteToList();
            return;
        }

        // Promt to keep done items if present
        if (list.itemsDone.value.length === 0) {
            convertListToNote(true);
        } else {
            Dialog.create({ component: NoteToChecklistDialog })
                .onOk(convertListToNote);
        }
    }

    /**
     * Move note to trash
     * @param {boolean} undone true if function call is undoing `restore()`
     */
    function trash(undone = false) {
        if (note.value.trashed > 0) {
            return;
        }
        note.value.setProps({ trashed: Date.now() });
        if (undone === false) {
            notify.action("Note trashed", restore);
        }
    }

    /** Update the note's geotag */
    async function updateGeotag() {
        let pos;
        try {
            pos = await geo.getPosition();
        } catch {
            notify.create({
                color: "negative",
                message: "Failed to get location permission"
            });
            return;
        }
        const coords = extend<GeolocationCoords>({}, pos.coords);
        note.value.setProps({ coords });
    }

    /** Overwrite geotag after prompting user */
    function promptOverwriteGeotag(parent?: LooseDictionary) {
        let message;
        if (note.value.coords) {
            const { latitude: lat, longitude: lon } = note.value.coords;
            message = `Currently set to: ${lat}, ${lon}`;
        } else {
            message = "Not currently set";
        }
        Dialog.create({
            title: "Overwrite current geotag?",
            message,
            cancel: true,
            parent
        }).onOk(updateGeotag);
    }

    // Listen to events
    if (watchServiceEvents) {
        useEventBus(Note, "updated", handleEvent);
        useEventBus(Note, "patched", handleEvent);
        useEventBus(Note, "removed", (e: ServiceEventWithContext) => handleEvent(e, true));
    }

    return {
        // Computes
        createdAtDisplay: computed(() => toDateDisplayString(note.value.createdAt)),
        inUseByForeignDevice: computed(() => state.foreignDeviceUsing),
        updatedAtDisplay: computed(() => toDateDisplayString(note.value.updatedAt)),
        positionDisplay: computed(() => {
            if (!note.value.coords) return "";
            const { latitude, longitude } = note.value.coords;
            return `${latitude.toFixed(6)}, ${longitude.toFixed(6)}`;
        }),
        // Methods
        archive,
        convertListToNote,
        convertNoteToList,
        deleteNote,
        duplicate,
        openLocation,
        promptDelete,
        promptOverwriteGeotag,
        restore,
        share,
        toggleList,
        trash,
        updateGeotag,
        // Delayable
        delayArchive: useDelay(delay, archive),
        delayDuplicate: useDelay(delay, duplicate),
        delayRestore: useDelay(delay, restore),
        delayToggleList: useDelay(delay, toggleList),
        delayTrash: useDelay(delay, trash),
        // useGeolocation
        obtainingPosition: geo.obtaining,
        // useList
        ...list
    }
}