import { isRef, Ref } from "@vue/composition-api";

/**
 * Create a function that will call the given function after a given delay
 * @param delay timeout
 * @param method method to call after delay
 */
export default function useDelay(delay: number | Ref<number>, method: Function): Function {
    return (...args: unknown[]) => {
        const d = isRef(delay) ? delay.value : delay;
        setTimeout(() => method(...args), d);
    }
}
