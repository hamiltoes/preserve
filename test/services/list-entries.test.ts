import assert from 'assert';
import app from '../../src-srv/app';

describe('\'list-entries\' service', () => {
  it('registered the service', () => {
    const service = app.service('list-entries');

    assert.ok(service, 'Registered the service');
  });
});
