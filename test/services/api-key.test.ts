import assert from 'assert';
import app from '../../src-srv/app';

describe('\'api-key\' service', () => {
  it('registered the service', () => {
    const service = app.service('api-key');

    assert.ok(service, 'Registered the service');
  });
});
