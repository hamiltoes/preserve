// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook } from "@feathersjs/feathers";
import { alterItems } from "feathers-hooks-common";

import { DataModel } from "../types";

export default (...fields: string[]): Hook => {
  return alterItems((rec: DataModel) => {
    for (const field of fields) {
      rec[field] = Date.now()
    }
  })
}
