// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { NotAuthenticated } from "@feathersjs/errors";
import { Hook, HookContext } from "@feathersjs/feathers";
import { omit } from "lodash";
import { getItems } from "feathers-hooks-common";

import { UserRecord, OwnedDataModel, IdType } from "../types"

interface HookContainer {
  /**
   * Ensure owner BEFORE service method
   * 
   * NOTE: Use for service `find`, `update`, `patch`, and `remove`.
   * - For `update`, `patch` and `remove`: get the desired item and validate userId field
   * - For `find`: add userId to query
   */
  before: Hook,
  /**
 * Ensure owner AFTER service method
 * 
 * NOTE: Only use for `get`
 */
  after: Hook
}

export default (userIdField = "userId"): HookContainer => {
  return {
    after(context: HookContext) {
      if (!context.params.user || !context.result || context.method !== "get") {
        return;
      }

      // Local Variables
      const userId = (context.params.user as UserRecord).id;
      const result: OwnedDataModel = context.result;

      // Check each item's userId
      if (result[userIdField] !== userId) {
        throw new NotAuthenticated();
      }
    },

    async before(context: HookContext) {

      // Allow server calls through
      if (!context.params.provider) {
        return;
      }

      // Require user be authenticated
      if (!context.params.authenticated) {
        throw new NotAuthenticated();
      }

      // Local Variables
      const userId = (context.params.user as UserRecord).id;

      // Just add userId to query on find() calls
      if (context.method === "find") {
        if (context.params.query) {
          context.params.query[userIdField] = userId;
        } else {
          context.params.query = { userId };
        }
        return;
      }

      // If data is available, we can do a quick check to ensure userId matches
      if (context.data) {
        const maybeArray = getItems(context);
        const items: OwnedDataModel[] = Array.isArray(maybeArray) ? maybeArray : [maybeArray];

        for (const item of items) {
          if (item[userIdField] && item[userIdField] !== userId) {
            throw new NotAuthenticated(`User '${userId}' cannot call method '${context.method}' on service '${context.path}' for User '${item[userIdField]}'`);
          }
        }
      }

      // We're done here if method is 'create'
      if (context.method === "create") {
        return;
      }

      // Check the original record in the DB to ensure user is not
      // maliciously (or accidentally) altering the userId
      //
      // NOTE: if AFTER hook runs on get() and userId is wrong, this call
      // will throw NotAuthenticated and the check below will be skipped
      const clientDataId = context.id as IdType;
      const originalRecord: OwnedDataModel = await context.service.get(clientDataId, omit(context.params, "provider"));

      // Check that the record's userId matches the authenticated user
      if (originalRecord[userIdField] !== userId) {
        throw new NotAuthenticated(`User '${userId}' cannot call method '${context.method}' on service '${context.path}' for User '${originalRecord[userIdField]}'`);
      }
    }
  }
}
