// Check API Key Permissions
// This hook must only run after the authenticate() hook has ran so this hook
// can access the
import { NotAuthenticated, TooManyRequests } from "@feathersjs/errors";
import { Hook, HookContext } from "@feathersjs/feathers";
import { pick } from "lodash";

import { isApiKeyPayload, ApiKeyRecord, isScopableService } from "../services/api-key/api-key.class";

/**
 * Checks API key scope
 * 
 * **This hook must run after the authenticate() hook** so that it has access to JWT payload
 */
export default (): Hook => {
  return async (ctx: HookContext) => {
    // Do not validate API key on internal calls
    if (!ctx.params.provider) {
      return ctx;
    }

    const serviceName: string = ctx.path;
    const method = ctx.method;
    const auth = ctx.params.authentication;
    if (!auth) {
      // During initial authentication and logout, allow the User to be retrieved
      if (ctx.params.authenticated && serviceName === "users" && method === "get" && ctx.params.user && ctx.params.user.id == ctx.id) {
        return ctx;
      }
      // This is likely because authenticate() did not run before this hook
      throw new NotAuthenticated("No auth info provided");
    }

    // Only run on JWT strategy
    if (auth.strategy !== "jwt") {
      return ctx;
    }

    // If auth has no `payload` or `payload` is not an API key, this token
    // was issued on a user:password login which has full privileges
    const payload = auth.payload;
    if (!payload || !isApiKeyPayload(payload)) {
      return ctx;
    }

    // Check if API key has permission to use this method on this service
    const scope = payload.scope;
    if (!isScopableService(serviceName) || !scope[serviceName].includes(method)) {
      throw new NotAuthenticated(`API key missing privilege '${method}' on '${serviceName}'`);
    }

    // Retrieve key metadata to check limits
    const tokenId = payload.jti;
    const service = ctx.app.service("api-key");
    const internalParams = pick(ctx.params, "authenticated", "user", "connection");
    let rec: ApiKeyRecord;
    try {
      rec = await service.get(tokenId, internalParams);
    } catch (e) {

      // If this key authenticated but does not exist in DB, it has been blacklisted
      if (e.code && e.code === 404) {
        throw new NotAuthenticated("API key is invalid");
      }

      // Re-throw: don't allow API key access unless we can actually validate it
      console.error(e);
      throw e;
    }

    // Check if API key has exceeded limit
    if (rec.hits >= payload.limit) {
      throw new TooManyRequests(`API key has exceeded ${payload.limit} call limit`);
    }

    // Update API key hit count and lastUsedAt timestamp
    try {
      await service.patch(tokenId, { hits: rec.hits + 1, lastUsedAt: Date.now() }, internalParams);
    } catch (e) {
      console.error(e)
    }

    return ctx;
  };
}
