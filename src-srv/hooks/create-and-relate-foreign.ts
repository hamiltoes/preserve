// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from "@feathersjs/feathers";
import { getItems } from "feathers-hooks-common";

import { DataModel } from "../types";
import { pick } from "lodash";

/** Configuration for creating a foreign entity that has this entity as a foreign key */
interface CreateAndRelateForeign {
  /** The name of the foreign key in the foreign entity that should be assigned the local ID */
  foreignKeyName: string;
  /** Name of foreign service */
  foreignService: string;
}

/**
 * Create a new setForeignWithLocal Hook
 * @param foreignServices Configurations for each foreign sevice to update
 */
export default (...foreignRelations: CreateAndRelateForeign[]): Hook => {

  return async function (ctx: HookContext<DataModel>) {
    // Only pass user in params so that foreign service events propogate back to this user
    const params = pick(ctx.params, "user");

    // Get items as array
    const maybeArray: DataModel | DataModel[] = getItems(ctx)
    const items = Array.isArray(maybeArray) ? maybeArray : [maybeArray];

    // Create all foreign entities that link to this item
    await Promise.all(foreignRelations.reduce(
      (all, { foreignKeyName: fk, foreignService: fs }) =>
        all.concat(items.map(item => ctx.app.service(fs).create({ [fk]: item.id }, params))),
      [] as Promise<DataModel>[]))
  }
}
