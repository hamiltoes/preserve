// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from "@feathersjs/feathers";
import { alterItems } from "feathers-hooks-common";

import { DataModel, StringIndexable } from "../types";
import { InternalEventContext } from "../channels";

/** Maps a local field to a foreign field */
interface LocalForeignMap {
  /** Name of foreign field */
  foreignField: string;
  /** Name of local field */
  localField: string;
}

/** Configuration for patching a foreign service with fields from this service */
interface SetForeignWithLocal {
  /** Name of local field which specifies the foreign ID */
  foreignIdField: string;
  /** Name of foreign service */
  foreignService: string;
  /** Get entry being removed from local service so we can still update foreign service?
   * 
   * More info: On a single item remove, there is no data. In order to update the foreign service, we
   * have to first obtain the local entity being removed in order to know the foreign ID and update
   * the foreign entity.
  */
  getLocalAndUpdateForeignOnRemove?: boolean;
  /** Local:foreign field maps */
  localForeignMaps?: LocalForeignMap[];
}

/**
 * Create a new setForeignWithLocal Hook
 * @param foreignServices Configurations for each foreign sevice to update
 */
export default (...foreignServices: SetForeignWithLocal[]): Hook => {
  return alterItems(async (rec: DataModel, context: HookContext) => {

    // Only pass user in params so that foreign service events propogate back to this user
    const internalEventContext: InternalEventContext = {
      kind: "hook",
      hook: "set-foreign-with-local"
    }
    const internalParams = {
      publishUserId: context.params.user.id,
      internalEventContext,
      connection: context.params.connection
    }

    for (const { foreignService, foreignIdField, getLocalAndUpdateForeignOnRemove = false, localForeignMaps = [] } of foreignServices) {
      const data: StringIndexable = {};
      const serv = context.app.service(foreignService);
      if (!serv) continue;

      // If removing a single item, fetch it if desired, otherwise we're done here
      if (!rec) {
        const isRemove = context.method === "remove";
        if (!getLocalAndUpdateForeignOnRemove || !isRemove || !context.id) continue;
        rec = await context.service.get(context.id);
      }

      // Can't update the foreign entity if we don't know its ID
      const foreignId = rec[foreignIdField];
      if (!foreignId) continue;

      // Copy local fields to foreign fields
      for (const { localField, foreignField } of localForeignMaps) {
        if (Object.prototype.hasOwnProperty.call(rec, localField)) {
          data[foreignField] = rec[localField];
        }
      }

      // Patch foreign
      serv.patch(foreignId, data, internalParams).catch(console.error);
    }
  })
}
