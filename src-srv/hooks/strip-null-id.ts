// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook } from "@feathersjs/feathers";
import { alterItems } from "feathers-hooks-common";

import { OwnedDataModel } from "../types";

export default (): Hook => {
  return alterItems((rec: Partial<OwnedDataModel>) => {
    if (rec.id === null) {
      delete rec.id;
    }
  })
}
