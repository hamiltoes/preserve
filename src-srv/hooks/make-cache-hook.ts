// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from "@feathersjs/feathers";
import { getItems } from "feathers-hooks-common";

/** HookContext with caching params */
interface CachingHookContext extends HookContext {
  /** Set when cache is hit */
  _cacheHit?: boolean;
}

/** Store interface used for caching */
export interface CacheStoreType<T> {

  /**
   * Retrieve item from cache
   * @param key item key
   * @returns desired item if it exists
   */
  get(key: string): T | undefined;

  /**
   * Store `value` in cache at `key`
   * @param key item key
   * @param value item to store
   * @param maxAge maximum age in cache
   * @returns item was stored
   */
  set(key: string, value: T, maxAge?: number): boolean;

  /**
 * Deletes item at `key` out of cache
 * @param key item key
 */
  delete(key: string): void;

  /**
   * Clear the cache entirely, throwing away all values.
   */
  clear(): void;
}

export interface MakeCacheHookOptions<T, K extends keyof T> {
  idField: K;
  clone?(item: T): T;
  makeCacheKey?(key: unknown, context: CachingHookContext): string;
};

/**
 * Create cacheHook
 * 
 * Only caches single items - NO query results (however each item in query result is added to cache)
 * Add hook to methods:
 * - BEFORE: get
 * - AFTER: all
 * 
 * @param store Store to use as cache
 * @param options cacheHook options
 */
export function makeCacheHook<T, K extends keyof T>(store: CacheStoreType<T>, options: MakeCacheHookOptions<T, K>): Hook {

  // Declare defaults
  function clone(item: T) {
    if (options.clone) {
      return options.clone(item);
    } else {
      return JSON.parse(JSON.stringify(item));
    }
  }
  function makeCacheKey(key: unknown, context: CachingHookContext): string {
    if (options.makeCacheKey) {
      return options.makeCacheKey(key, context);
    } else {
      return `${context.path}/${key}`;
    }
  }

  // Determine what is used based on options
  const idField = options.idField;

  return (context: CachingHookContext) => {

    if (context.type === "before" && context.method === "get" && context.id) {
      // Handle BEFORE GET
      const key = makeCacheKey(context.id, context);
      const value = store.get(key);
      // console.debug('get: ', key, value)

      // Set result and let `after` hook know to do nothing
      if (value) {
        context._cacheHit = true;
        context.result = value;
      }

    } else if (context.type === "after") {
      // Handle AFTER hooks

      // Skip setting item in cache when all we're doing is returning what we retrieved from the cache
      if (context._cacheHit) return context;
      // Don't store `$select`ed items
      const $select = (context.params.query || {}).$select;
      if (context.method === "find" && $select) return context;

      // Special case for After REMOVE: just remove from cache
      if (context.method === "remove") {
        if (context.id) {
          const key = makeCacheKey(context.id, context);
          store.delete(key);
          // console.debug('delete: ', key)
        }
      } else {
        const itemsAmbiguous: T | T[] = getItems(context);
        const items: T[] = Array.isArray(itemsAmbiguous) ? itemsAmbiguous : [itemsAmbiguous];

        // Store all items in cache
        items.forEach(item => {
          const key = makeCacheKey(item[idField], context)
          const itemClone = clone(item);
          store.set(key, itemClone)
          // console.debug('set: ', key, itemClone)
        });
      }

    } // END context.type === after

    // Always return context
    return context;
  };
}
