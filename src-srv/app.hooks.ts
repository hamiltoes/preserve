// Application hooks that run for every service
import { GeneralError } from "@feathersjs/errors";
import { HookContext } from "@feathersjs/feathers";

import stripNullId from "./hooks/strip-null-id";
// Don't remove this comment. It's needed to format import lines nicely.

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [stripNullId()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [(ctx: HookContext) => {
      if (ctx.error) {
        // Catch uncaught server errors and return 500 to client
        if (!ctx.error.code) {
          ctx.error = new GeneralError("server error");
          return ctx;
        }
        // Hide stack trace in production
        if (ctx.error.code === 404 || process.env.NODE_ENV === "production") {
          ctx.error.stack = null;
        }
        return ctx;
      }
    }],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
