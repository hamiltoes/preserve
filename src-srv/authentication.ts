import { ServiceAddons } from "@feathersjs/feathers";
import { AuthenticationService, JWTStrategy } from "@feathersjs/authentication";
import { LocalStrategy } from "@feathersjs/authentication-local";
import { expressOauth } from "@feathersjs/authentication-oauth";
import { paramsFromClient } from "feathers-hooks-common";

import { Application } from "./declarations";

export interface DeviceDetails {
  uid: string,
  details: { [index in string]: any };
}

declare module "./declarations" {
  interface ServiceTypes {
    "authentication": AuthenticationService & ServiceAddons<unknown>;
  }
}

// Augment $deviceDetails to params.connection type
declare module "@feathersjs/transport-commons/lib/channels/channel/base" {
  interface RealTimeConnection {
    $deviceDetails?: DeviceDetails
  }
}

export default function (app: Application) {
  const authentication = new AuthenticationService(app);

  authentication.register("jwt", new JWTStrategy());
  authentication.register("local", new LocalStrategy());

  app.use("/authentication", authentication);
  app.configure(expressOauth());

  // Extract `deviceDetails` from client if present
  app.service("authentication").hooks({
    before: {
      create: [
        paramsFromClient("deviceDetails"),
        ctx => {
          // Add deviceDetails to connection
          const deviceDetails = ctx.params.deviceDetails as DeviceDetails;
          if (deviceDetails && ctx.params.connection) {
            ctx.params.connection.$deviceDetails = deviceDetails
          }
        }
      ]
    }
  })
}
