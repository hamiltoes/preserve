
import { default as _LRUCache } from "lru-cache";

/** Cache implementing Least-Recently Used eviction policy */
export class LRUCache<K, V> extends _LRUCache<K, V> {

    /** Deletes a key out of the cache */
    public delete(key: K): void {
        return this.del(key);
    }

    /** Clear the cache entirely, throwing away all values */
    public clear(): void {
        return this.reset();
    }
}
