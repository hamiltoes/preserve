import * as authentication from "@feathersjs/authentication";
import { disallow, validateSchema } from "feathers-hooks-common";

import checkApiKey from "../../hooks/check-api-key";
import makeEnsureOwner from "../../hooks/ensure-owner";
import { makeCacheHook } from "../../hooks/make-cache-hook";
import { LRUCache } from "../../utl";
import { ajv, apiKeyCreate, apiKeyUpdate } from "../../schemas";
import { OwnedDataModel } from "../../types"
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

// Set up cache hook
const cacheMap = new LRUCache<string, OwnedDataModel>({ max: 100 });
const cacheHook = makeCacheHook(cacheMap,
  {
    idField: "id",
    // A shallow clone is sufficient for api-keys (payload is JSONified)
    clone(item: OwnedDataModel) { return { ...item } }
  }
);

const ensureOwner = makeEnsureOwner();

export default {
  before: {
    all: [authenticate("jwt"), checkApiKey()],
    find: [ensureOwner.before],
    get: [cacheHook],
    create: [ensureOwner.before, validateSchema(apiKeyCreate, ajv)],
    update: [disallow("external"), validateSchema(apiKeyUpdate, ajv)],
    patch: [disallow("external"), validateSchema(apiKeyUpdate, ajv)],
    remove: [ensureOwner.before]
  },

  after: {
    all: [cacheHook],
    find: [],
    get: [ensureOwner.after],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
