import { AuthenticationService } from "@feathersjs/authentication/lib";
import { Params, HookContext } from "@feathersjs/feathers";
import { Service, KnexServiceOptions } from "feathers-knex";

import { Application } from "../../declarations";
import { UserRecord, OwnedDataModelBase } from "../../types";

type ServiceMethodName = HookContext["method"];

enum ScopableServices {
  "notes" = "notes",
  "list-entries" = "list-entries"
}
type ApiKeyScopeServices = keyof typeof ScopableServices;

export function isScopableService(s: string): s is ApiKeyScopeServices {
  return s in ScopableServices
}

export type ApiKeyScope = {
  [state in ScopableServices]: ServiceMethodName[]
}

interface CreateApiKeyOptions {
  description: string,
  expiresIn: number,
  scope: ApiKeyScope,
  limit: number,
  userId: UserRecord["id"]
}

export interface JwtAuthPayload {
  /** API key ID */
  jti: string,
  /** Issued at (epoch seconds) */
  iat: number,
  /** Expires at (epoch seconds) */
  exp: number,
  /** JWT subject: owning user's ID (as string) */
  sub: string
}

export interface ApiKeyPayload extends JwtAuthPayload {
  /** This payload is an API key */
  isApiKey: true,
  /** Number of hits permitted on this API key */
  limit: number,
  /** Description of this API key */
  description: string,
  /** API key permissions */
  scope: ApiKeyScope,
}

export type JwtPayload = JwtAuthPayload | ApiKeyPayload;

/** ApiKeyPayload type guard */
export function isApiKeyPayload(token: JwtPayload): token is ApiKeyPayload {
  return (token as ApiKeyPayload).isApiKey === true;
}

export interface ApiKeyRecord extends OwnedDataModelBase {
  /** API key ID */
  id: string,
  /** The actual JWT API key */
  accessToken: string,
  /** Expires at (epoch milliseconds) */
  expiresAt: number,
  /** Number of times API key has been used */
  hits: number,
  /** Issued at (epoch milliseconds) */
  issuedAt: number,
  /** JWT payload as JSONified string */
  payload: ApiKeyPayload,
  /** Last used at (epoch milliseconds) */
  lastUsedAt: number,
}

export class ApiKey extends Service<ApiKeyRecord> {
  app: Application;
  authentication: AuthenticationService;

  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    super({
      ...options,
      name: "api_key"
    });
    this.app = app;
    this.authentication = app.service("authentication");
  }

  async create(data: CreateApiKeyOptions[], params: Params): Promise<ApiKeyRecord[]>;
  async create(data: CreateApiKeyOptions, params: Params): Promise<ApiKeyRecord>;
  async create(data: CreateApiKeyOptions | CreateApiKeyOptions[], params: Params): Promise<ApiKeyRecord | ApiKeyRecord[]> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }
    // Assign expiration
    if (!params.jwtOptions) {
      params.jwtOptions = {};
    }
    params.jwtOptions.expiresIn = data.expiresIn;

    // Get token options
    const options = await this.authentication.getTokenOptions({ user: params.user }, params);

    // Get JWT
    const payload: Partial<ApiKeyPayload> = {
      isApiKey: true,
      scope: data.scope,
      limit: data.limit,
      description: data.description || ""
    };
    const accessToken = await this.authentication.createAccessToken(payload, options);
    const finalPayload: ApiKeyPayload = await this.authentication.verifyAccessToken(accessToken);

    // Massage into record for DB
    const record: ApiKeyRecord = {
      id: finalPayload.jti, // Use token's UUID as ID
      accessToken,
      userId: data.userId,
      expiresAt: finalPayload.exp * 1000,
      hits: 0,
      issuedAt: finalPayload.iat * 1000,
      lastUsedAt: 0,
      payload: finalPayload,
    }
    return super.create(record);
  }
}
