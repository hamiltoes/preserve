import { Service, KnexServiceOptions } from "feathers-knex";
import { Application } from "../../declarations";

import { OwnedDataModelBase, CreatedAndUpdatedTracker } from "../../types";

export type KeepColor = "default" |
  "red" |
  "orange" |
  "yellow" |
  "green" |
  "teal" |
  "blue" |
  "darkblue" |
  "purple" |
  "pink" |
  "brown" |
  "gray";

export interface NoteRecord extends OwnedDataModelBase, CreatedAndUpdatedTracker {
  /** Note ID */
  id: number;
  /** Note is archived */
  archived: boolean;
  /** Note is list */
  checkboxes: boolean;
  /** Note color */
  color: KeepColor;
  /** Note's geotag as stringified JSON */
  coords: string;
  /** Note is starred */
  starred: boolean;
  /** Note text */
  text: string;
  /** Note title */
  title: string;
  /** Trashed at (epoch milliseconds) */
  trashed: number;
}

export class Notes extends Service<NoteRecord> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    super({
      ...options,
      name: "notes"
    });
  }
}
