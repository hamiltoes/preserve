import { Application } from "../declarations";
import users from "./users/users.service";
import notes from "./notes/notes.service";
import listEntries from "./list-entries/list-entries.service";
import apiKey from "./api-key/api-key.service";
import userSettings from "./user-settings/user-settings.service";
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(users);
  app.configure(notes);
  app.configure(listEntries);
  app.configure(apiKey);
  app.configure(userSettings);
}
