import * as authentication from "@feathersjs/authentication";
import { validateSchema } from "feathers-hooks-common";

import checkApiKey from "../../hooks/check-api-key";
import makeEnsureOwner from "../../hooks/ensure-owner";
import { makeCacheHook } from "../../hooks/make-cache-hook";
import setForeignWithLocal from "../../hooks/set-foreign-with-local";
import { LRUCache } from "../../utl";
import { ajv, listEntry as listEntrySchema } from "../../schemas";
import { OwnedDataModel } from "../../types"
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

// Set up cache hook
const cacheMap = new LRUCache<string, OwnedDataModel>({ max: 100 });
const cacheHook = makeCacheHook(cacheMap,
  {
    idField: "id",
    // A shallow clone is sufficient for list-entries
    clone(item: OwnedDataModel) { return { ...item } }
  }
);

const ensureOwner = makeEnsureOwner();

const setForeign = setForeignWithLocal({
  foreignService: "notes",
  foreignIdField: "noteId",
  getLocalAndUpdateForeignOnRemove: true
  // localForeignMaps: []
  // Intentionally not specifying localForeignMaps. I just want notes' setNow hook to update its editedAt timestamp 
})

export default {
  before: {
    all: [authenticate("jwt"), checkApiKey()],
    find: [ensureOwner.before],
    get: [cacheHook],
    create: [ensureOwner.before, validateSchema(listEntrySchema, ajv), setForeign],
    update: [ensureOwner.before, validateSchema(listEntrySchema, ajv), setForeign],
    patch: [ensureOwner.before, validateSchema(listEntrySchema, ajv), setForeign],
    remove: [ensureOwner.before, setForeign]
  },

  after: {
    all: [cacheHook],
    find: [],
    get: [ensureOwner.after],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
