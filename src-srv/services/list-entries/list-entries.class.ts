import { Service, KnexServiceOptions } from "feathers-knex";
import { Application } from "../../declarations";

import { OwnedDataModelBase, NoteRecord } from "../../types";

export interface ListEntryRecord extends OwnedDataModelBase {
  /** List Entry ID */
  id: number;
  /** List Entry note's ID */
  noteId: NoteRecord["id"];
  /** List entry is done */
  done: boolean;
  /** List entry text */
  text: string;
}

export class ListEntries extends Service<ListEntryRecord> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    super({
      ...options,
      name: "list_entries"
    });
  }
}
