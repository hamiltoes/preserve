// Initializes the `list-entries` service on path `/list-entries`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
import { ListEntries, ListEntryRecord } from "./list-entries.class";
import { createModel } from "../../models/list-entries.model";
import hooks from "./list-entries.hooks";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    "list-entries": ListEntries & ServiceAddons<ListEntryRecord>;
  }
}

export default function (app: Application) {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
    multi: ["create"]
  };

  // Initialize our service with any options it requires
  app.use("/list-entries", new ListEntries(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("list-entries");

  service.hooks(hooks);
}
