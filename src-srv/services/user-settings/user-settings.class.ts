import { Service, KnexServiceOptions } from "feathers-knex";
import { Application } from "../../declarations";
import { OwnedDataModelBase } from "../../types";

/** User Settings model */
export interface UserSettingsRecord extends OwnedDataModelBase {
  /** User Settings unique ID */
  id: number;

  /** Allow expanding lists in preview? */
  allowExpandChecklistInPreview: boolean;

  /** Show Grid layout? (vs. List layout) */
  displayGrid: boolean;

  /** Enable dark theme? */
  darkTheme: boolean;

  /** Allow geotagging notes? */
  geotagNotes: boolean;
}

export class UserSettings extends Service<UserSettingsRecord> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    super({
      ...options,
      name: "user_settings"
    });
  }
}
