import { Service, KnexServiceOptions } from "feathers-knex";

import { Application } from "../../declarations";
import { DataModelBase, CreatedAndUpdatedTracker } from "../../types";

/** User Data Model */
export interface UserRecord extends DataModelBase, CreatedAndUpdatedTracker {
  /** user's unique ID */
  id: number;

  /** user's email address */
  email: string;

  /** user's (hashed) password */
  password?: string;
}

export class Users extends Service<UserRecord> {
  app: Application;

  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    super({
      ...options,
      name: "users"
    });
    this.app = app;
  }
}
