import { HookContext, Query } from "@feathersjs/feathers";
import { fastJoin, ResolverMap, SyncContextFunction } from "feathers-hooks-common";

import { UserWithSettings } from "../../types";

export const resolvers: ResolverMap<UserWithSettings> = {
    joins: {
        settings: () => async (user, ctx) => {
            let resp;
            try {
                resp = await ctx.app.service("user-settings").find({ query: { userId: user.id } });
            } catch (error) {
                console.error("Error getting user's settings: ", error);
                return;
            }
            let settings;
            if (Array.isArray(resp)) {
                settings = resp[0];
            } else {
                settings = resp.data[0];
            }
            if (settings) {
                user.settings = settings;
            }
        }
    }
}

export const query: SyncContextFunction<Query> = (ctx: HookContext<UserWithSettings>) => ({
    settings: !!ctx.params.provider
})

export default fastJoin(resolvers, query)
