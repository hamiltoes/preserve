import * as feathersAuthentication from "@feathersjs/authentication";
import * as local from "@feathersjs/authentication-local";
import { validateSchema } from "feathers-hooks-common";
import { HookContext } from "@feathersjs/feathers";
import { Conflict } from "@feathersjs/errors";

import populateUser from "./users.populate";
import checkApiKey from "../../hooks/check-api-key";
import createAndRelateForeign from "../../hooks/create-and-relate-foreign";
import makeEnsureOwner from "../../hooks/ensure-owner";
import { makeCacheHook } from "../../hooks/make-cache-hook";
import setNow from "../../hooks/set-now";
import { LRUCache } from "../../utl";
import { ajv, user as userSchema } from "../../schemas";
import { DataModel } from "../../types"
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;

// Set up cache hook
const cacheMap = new LRUCache<string, DataModel>({ max: 100 });
const cacheHook = makeCacheHook(cacheMap,
  {
    idField: "id",
    // A shallow clone is sufficient for users
    clone(item: DataModel) { return { ...item } }
  }
);

const ensureOwner = makeEnsureOwner("id");

export default {
  before: {
    all: [],
    find: [authenticate("jwt"), checkApiKey(), ensureOwner.before],
    get: [authenticate("jwt"), checkApiKey(), cacheHook],
    create: [hashPassword("password"), validateSchema(userSchema, ajv), setNow("createdAt", "updatedAt")],
    update: [
      hashPassword("password"),
      authenticate("jwt"),
      checkApiKey(),
      ensureOwner.before,
      validateSchema(userSchema, ajv),
      setNow("updatedAt")
    ],
    patch: [
      hashPassword("password"),
      authenticate("jwt"),
      checkApiKey(),
      ensureOwner.before,
      validateSchema(userSchema, ajv),
      setNow("updatedAt")
    ],
    remove: [authenticate("jwt"), checkApiKey(), ensureOwner.before]
  },

  after: {
    all: [
      cacheHook,
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect("password")
    ],
    find: [],
    get: [ensureOwner.after, populateUser],
    create: [
      createAndRelateForeign(
        { foreignService: "user-settings", foreignKeyName: "userId" }
      )
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [(ctx: HookContext) => {
      if (ctx.error) {
        if (ctx.error.message.toLowerCase().indexOf("unique") >= 0) {
          ctx.error = new Conflict("Email is already in use");
        }
        return ctx;
      }
    }],
    update: [],
    patch: [],
    remove: []
  }
};
