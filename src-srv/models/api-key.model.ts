// api-key-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
import Knex from "knex";
import { Application } from "../declarations";

export default function setupTable(app: Application) {
  const db: Knex = app.get("knexClient");
  const tableName = "api_key";
  return db.schema.hasTable(tableName).then(exists => {
    if (!exists) {
      return db.schema.createTable(tableName, table => {
        table.string("id").primary();
        table.integer("userId").notNullable();
        table.foreign("userId").references("id").inTable("users").onDelete("CASCADE");
        table.text("accessToken");
        table.bigInteger("expiresAt");
        table.integer("hits");
        table.bigInteger("issuedAt");
        table.bigInteger("lastUsedAt");
        table.json("payload");
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });
}

export function createModel(app: Application) {
  return app.get("knexClient") as Knex;
}
