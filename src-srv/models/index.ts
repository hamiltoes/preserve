import { Application } from "../declarations";
import setupApiKeys from "./api-key.model";
import setupListEntries from "./list-entries.model";
import setupNotes from "./notes.model";
import setupUserSettings from "./user-settings.model";
import setupUsers from "./users.model";

export default function (app: Application) {
    app.on("listening", () => {
        // Users must be setup first
        setupUsers(app).then(() => {
            // Users FK
            setupUserSettings(app)
            setupApiKeys(app)
            setupNotes(app).then(() => {
                // Notes FK
                setupListEntries(app)
            })
        })
    })
}
