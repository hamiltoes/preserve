// users-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
import { Application } from "../declarations";
import Knex from "knex";

export default function setupTable(app: Application) {
  const db: Knex = app.get("knexClient");
  const tableName = "users";
  return db.schema.hasTable(tableName).then(exists => {
    if (!exists) {
      return db.schema.createTable(tableName, table => {
        table.increments("id").primary();
        table.bigInteger("createdAt");
        table.string("email").unique();
        table.string("password");
        table.bigInteger("updatedAt");
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });
}

export function createModel(app: Application) {
  return app.get("knexClient") as Knex;
}
