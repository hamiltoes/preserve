/** Unique identifier type */
export type IdType = number | string;

/** String-Indexable Object */
export interface StringIndexable {
    [key: string]: unknown;
}

export interface DataModelBase {
    /** unique ID */
    id: IdType;
}

/** Generic Data Model */
export type DataModel = DataModelBase & StringIndexable;

/** Milliseconds since Jan. 1, 1970 */
export type epochTime = number;

/** Tracks `createdAt` time */
export interface CreatedTracker {
    /** epoch time of creation */
    createdAt: epochTime;
}
/** Tracks `updatedAt` time */
export interface UpdatedTracker {
    /** epoch time of last update */
    updatedAt: epochTime;
}

/** Tracks 'createdAt` and `updatedAt` */
export type CreatedAndUpdatedTracker = CreatedTracker & UpdatedTracker;

import { UserRecord } from "../services/users/users.class";
export { UserRecord }

export interface OwnedDataModelBase extends DataModelBase {
    /** owning user's unique ID */
    userId: UserRecord["id"];
}

/** Generic Data Model owned by User */
export type OwnedDataModel = OwnedDataModelBase & StringIndexable;

import { UserSettingsRecord } from "../services/user-settings/user-settings.class";
export { UserSettingsRecord }

/** User with User Settings */
export interface UserWithSettings extends UserRecord {
    /** user's settings */
    settings?: UserSettingsRecord;
}

export { NoteRecord, KeepColor } from "../services/notes/notes.class";

export { ListEntryRecord } from "../services/list-entries/list-entries.class";

export { ApiKeyRecord, ApiKeyPayload, JwtAuthPayload, JwtPayload as IJwtPayload } from "../services/api-key/api-key.class";
