import Ajv from "ajv";
const ajv = new Ajv({
    allErrors: true,
    $data: true,
    removeAdditional: "all" // Will prune all fields not specified in schemas
});

import apiKeyCreate from "./api-key-create.schema.json";
import apiKeyScope from "./api-key-scope.schema.json";
import apiKeyUpdate from "./api-key-update.schema.json";
import booly from "./booly.schema.json";
import epochDatetime from "./epochDatetime.schema.json";
import keepColor from "./keepColor.schema.json";
import listEntry from "./list-entry.schema.json";
import note from "./note.schema.json";
import numberOrNull from "./number-or-null.schema.json";
import user from "./user.schema.json";

// Add schemas
ajv.addSchema(apiKeyCreate);
ajv.addSchema(apiKeyScope);
ajv.addSchema(apiKeyUpdate);
ajv.addSchema(booly);
ajv.addSchema(epochDatetime);
ajv.addSchema(keepColor);
ajv.addSchema(listEntry);
ajv.addSchema(note);
ajv.addSchema(numberOrNull);
ajv.addSchema(user);

export { ajv, apiKeyCreate, apiKeyScope, apiKeyUpdate, listEntry, note, user };
