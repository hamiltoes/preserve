import "@feathersjs/transport-commons";
import { HookContext, Params } from "@feathersjs/feathers";
import { Application } from "./declarations";
import { DataModel, UserRecord } from "./types";
import { DeviceDetails } from "./authentication";
import { AuthenticationResult } from "@feathersjs/authentication/lib";

interface ChoreEventContext {
  kind: "chore"
  chore: "empty-trash"
}

interface HookEventContext {
  kind: "hook"
  hook: "set-foreign-with-local"
}

export type InternalEventContext = ChoreEventContext | HookEventContext;

export interface ServiceEventWithContext {
  data: DataModel,
  $context: {
    device?: DeviceDetails,
    internal?: InternalEventContext,
    time: number,
    userId?: UserRecord["id"]
  }
}

export default function (app: Application) {
  if (typeof app.channel !== "function") {
    // If no real-time functionality has been configured just return
    return;
  }

  app.on("connection", (connection: Params["connection"]) => {
    // On a new real-time connection, add it to the anonymous channel
    if (connection) {
      app.channel("anonymous").join(connection);
    }
  });

  app.on("login", (authResult: AuthenticationResult, { connection }: { connection: Params["connection"] }) => {
    // connection can be undefined if there is no
    // real-time connection, e.g. when logging in via REST
    if (connection) {
      // Obtain the logged in user from the connection
      // const user = connection.user;

      // The connection is no longer anonymous, remove it
      app.channel("anonymous").leave(connection);

      // Add it to the authenticated user channel
      app.channel("authenticated").join(connection);

      // Channels can be named anything and joined on any condition 

      // E.g. to send real-time events only to admins use
      // if(user.isAdmin) { app.channel('admins').join(connection); }

      // If the user has joined e.g. chat rooms
      // if(Array.isArray(user.rooms)) user.rooms.forEach(room => app.channel(`rooms/${room.id}`).join(channel));

      // Easily organize users by email and userid for things like messaging
      // app.channel(`emails/${user.email}`).join(channel);
      app.channel(`users/${connection.user.id}`).join(connection);
    }
  });

  // eslint-disable-next-line no-unused-vars
  app.publish((data: DataModel, ctx: HookContext) => {
    // Only publish events to the user who created this event but don't publish to the source connection
    // (publish to this user's other devices)
    const userId = ctx.params.user?.id || ctx.params.publishUserId;
    if (userId) {
      const withCtx: ServiceEventWithContext = {
        data,
        $context: {
          device: ctx.params.connection && ctx.params.connection.$deviceDetails,
          internal: ctx.params.internalEventContext,
          time: Date.now(),
          userId
        }
      }
      let channels = app.channel(`users/${userId}`);
      // Filter source connection if it's not an internal event
      if (!ctx.params.publishUserId) {
        channels = channels.filter(connection => connection !== ctx.params.connection);
      }
      return channels.send(withCtx)
    }
  });

  // Here you can also add service specific event publishers
  // e.g. the publish the `users` service `created` event to the `admins` channel
  // app.service('users').publish('created', () => app.channel('admins'));

  // With the userid and email organization from above you can easily select involved users
  // app.service('messages').publish(() => {
  //   return [
  //     app.channel(`userIds/${data.createdBy}`),
  //     app.channel(`emails/${data.recipientEmail}`)
  //   ];
  // });
};
