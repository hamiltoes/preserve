import { Application } from "../declarations";
import emptyTrash from "./empty-trash.chore";

export default function (app: Application) {
    app.configure(emptyTrash)
}
