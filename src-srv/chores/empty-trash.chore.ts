/**
 * This chore purges notes that have been in the trash for >7 days
 * for *all* users.
 */
import { Application } from "../declarations";
import { InternalEventContext } from "../channels";

const maxAge = 7 * 24 * 60 * 60 * 1000; // 7 days
const period = 5 * 60 * 1000; // 5 minutes

export default function (app: Application) {
    const notes = app.service("notes");

    async function emptyTrash() {
        try {
            const paginated = await notes.find({
                query: {
                    trashed: {
                        $ne: 0,
                        $lt: Date.now() - maxAge
                    },
                    $limit: 50
                }
            });
            const toDelete = Array.isArray(paginated) ? paginated : paginated.data;
            if (toDelete.length) {
                const internalEventContext: InternalEventContext = {
                    kind: "chore",
                    chore: "empty-trash"
                }
                await Promise.all(toDelete.map(n => notes.remove(n.id, {
                    publishUserId: n.userId,
                    internalEventContext
                })));
            }
        } catch (e) {
            console.error("Error emptying trash:", e)
        }
    }

    setInterval(() => { emptyTrash() }, period)
}
